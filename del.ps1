"---------------------------------------------------------"
Get-ChildItem -Path (Get-Location) -Exclude "backup.bat","del.bat","del.ps1","excludelist.txt","Rar.exe","rarreg.key","src" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
   Get-ChildItem -Path "src\EquipmentSystem.AngularClient" -Exclude "node_modules" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.Common" -Exclude "bin","obj" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.ConsoleClient" -Exclude "bin","obj" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.DataLayer" -Exclude "bin","obj" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.DomainClasses" -Exclude "bin","obj" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.Services" -Exclude "bin","obj" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"
Get-ChildItem -Path "src\EquipmentSystem.WebApp" -Exclude "bin","obj","wwwroot" | foreach ($_) {
       "CLEANING :" + $_.fullname
       Remove-Item $_.fullname -Force -Recurse
       "CLEANED... :" + $_.fullname
   }
"---------------------------------------------------------"