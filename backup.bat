@echo off
rem rmdir /S /Q bin
rem rmdir /S /Q obj

for %%I in (.) do set CurrDirName=%%~nxI
rem echo %CurrDirName%

rem set _my_datetime=%date%_%time%
rem set _my_datetime=%_my_datetime: =_%
rem set _my_datetime=%_my_datetime::=%
rem set _my_datetime=%_my_datetime:/=_%
rem set _my_datetime=%_my_datetime:.=_%

For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c%%a%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME: =0%") do (set mytime=%%a%%b)

md %USERPROFILE%\Desktop\%CurrDirName%
XCOPY *.* %USERPROFILE%\Desktop\%CurrDirName% /S /I /Y /EXCLUDE:excludelist.txt

"Rar.exe" a -ep1 -r %USERPROFILE%\Desktop\%CurrDirName%_%mydate%_%mytime%  %USERPROFILE%\Desktop\%CurrDirName%
rmdir /S /Q %USERPROFILE%\Desktop\%CurrDirName%
pause...