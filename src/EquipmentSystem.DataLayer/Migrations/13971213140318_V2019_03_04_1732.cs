﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_03_04_1732 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.AlterColumn<int>(
                name: "UserEquip_ID",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs",
                column: "UserEquip_ID",
                principalTable: "User_Equipments",
                principalColumn: "UserEquip_ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.AlterColumn<int>(
                name: "UserEquip_ID",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs",
                column: "UserEquip_ID",
                principalTable: "User_Equipments",
                principalColumn: "UserEquip_ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
