﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_02_04_2356 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "User_UserName",
                table: "Users",
                newName: "Username");

            migrationBuilder.RenameColumn(
                name: "User_Password",
                table: "Users",
                newName: "Password");

            migrationBuilder.RenameIndex(
                name: "IX_Users_User_UserName",
                table: "Users",
                newName: "IX_Users_Username");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Username",
                table: "Users",
                newName: "User_UserName");

            migrationBuilder.RenameColumn(
                name: "Password",
                table: "Users",
                newName: "User_Password");

            migrationBuilder.RenameIndex(
                name: "IX_Users_Username",
                table: "Users",
                newName: "IX_Users_User_UserName");
        }
    }
}
