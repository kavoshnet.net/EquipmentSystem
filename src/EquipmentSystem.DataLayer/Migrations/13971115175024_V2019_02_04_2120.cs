﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_02_04_2120 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Equipments",
                columns: table => new
                {
                    Equip_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Equip_Name = table.Column<string>(maxLength: 255, nullable: false),
                    Parent_Equip_ID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipments", x => x.Equip_ID);
                    table.ForeignKey(
                        name: "FK_Equipments_Equipments_Parent_Equip_ID",
                        column: x => x.Parent_Equip_ID,
                        principalTable: "Equipments",
                        principalColumn: "Equip_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Offices",
                columns: table => new
                {
                    Office_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Office_Name = table.Column<string>(maxLength: 255, nullable: false),
                    Parent_Office_ID = table.Column<int>(nullable: true),
                    Office_Code = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offices", x => x.Office_ID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Role_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Role_Name = table.Column<string>(maxLength: 450, nullable: false),
                    Role_Caption = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Role_ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    User_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    User_Fname = table.Column<string>(maxLength: 255, nullable: false),
                    User_Lname = table.Column<string>(maxLength: 255, nullable: false),
                    User_UserName = table.Column<string>(maxLength: 450, nullable: false),
                    User_Password = table.Column<string>(maxLength: 255, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    LastLoggedIn = table.Column<DateTimeOffset>(nullable: false),
                    SerialNumber = table.Column<string>(maxLength: 450, nullable: false),
                    Office_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.User_ID);
                    table.ForeignKey(
                        name: "FK_Users_Offices_Office_ID",
                        column: x => x.Office_ID,
                        principalTable: "Offices",
                        principalColumn: "Office_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Equipments",
                columns: table => new
                {
                    UserEquip_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SHSerial = table.Column<string>(maxLength: 255, nullable: false),
                    SHAmval = table.Column<string>(maxLength: 255, nullable: false),
                    Desc1 = table.Column<string>(maxLength: 255, nullable: true),
                    Desc2 = table.Column<string>(maxLength: 255, nullable: true),
                    Desc3 = table.Column<string>(maxLength: 255, nullable: true),
                    Desc4 = table.Column<string>(maxLength: 255, nullable: true),
                    Desc5 = table.Column<string>(maxLength: 255, nullable: true),
                    Desc6 = table.Column<string>(maxLength: 255, nullable: true),
                    User_ID = table.Column<int>(nullable: false),
                    Equip_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Equipments", x => x.UserEquip_ID);
                    table.ForeignKey(
                        name: "FK_User_Equipments_Equipments_Equip_ID",
                        column: x => x.Equip_ID,
                        principalTable: "Equipments",
                        principalColumn: "Equip_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Equipments_Users_User_ID",
                        column: x => x.User_ID,
                        principalTable: "Users",
                        principalColumn: "User_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    User_ID = table.Column<int>(nullable: false),
                    Role_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.User_ID, x.Role_ID });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_Role_ID",
                        column: x => x.Role_ID,
                        principalTable: "Roles",
                        principalColumn: "Role_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_User_ID",
                        column: x => x.User_ID,
                        principalTable: "Users",
                        principalColumn: "User_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserToken_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessTokenHash = table.Column<string>(nullable: true),
                    AccessTokenExpiresDateTime = table.Column<DateTimeOffset>(nullable: false),
                    RefreshTokenIdHash = table.Column<string>(maxLength: 450, nullable: false),
                    RefreshTokenIdHashSource = table.Column<string>(maxLength: 450, nullable: true),
                    RefreshTokenExpiresDateTime = table.Column<DateTimeOffset>(nullable: false),
                    User_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => x.UserToken_ID);
                    table.ForeignKey(
                        name: "FK_UserTokens_Users_User_ID",
                        column: x => x.User_ID,
                        principalTable: "Users",
                        principalColumn: "User_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Equipments_Parent_Equip_ID",
                table: "Equipments",
                column: "Parent_Equip_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_Role_Name",
                table: "Roles",
                column: "Role_Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_Equipments_Equip_ID",
                table: "User_Equipments",
                column: "Equip_ID");

            migrationBuilder.CreateIndex(
                name: "IX_User_Equipments_User_ID",
                table: "User_Equipments",
                column: "User_ID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_Role_ID",
                table: "UserRoles",
                column: "Role_ID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_User_ID",
                table: "UserRoles",
                column: "User_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Office_ID",
                table: "Users",
                column: "Office_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_User_UserName",
                table: "Users",
                column: "User_UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserTokens_User_ID",
                table: "UserTokens",
                column: "User_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User_Equipments");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "Equipments");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Offices");
        }
    }
}
