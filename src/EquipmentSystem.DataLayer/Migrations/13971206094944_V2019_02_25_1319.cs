﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_02_25_1319 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_Equipments_EquipReq_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.AlterColumn<int>(
                name: "UserReq_ID",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserRecAction",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserDo_ID",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserDoAction",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EquipReq_ID",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "DateReqAction",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(DateTimeOffset));

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "DateDoAction",
                table: "User_EquipmentReqs",
                nullable: true,
                oldClrType: typeof(DateTimeOffset));

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_Equipments_EquipReq_ID",
                table: "User_EquipmentReqs",
                column: "EquipReq_ID",
                principalTable: "Equipments",
                principalColumn: "Equip_ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_Equipments_EquipReq_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.AlterColumn<int>(
                name: "UserReq_ID",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserRecAction",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserDo_ID",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserDoAction",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EquipReq_ID",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "DateReqAction",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "DateDoAction",
                table: "User_EquipmentReqs",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_Equipments_EquipReq_ID",
                table: "User_EquipmentReqs",
                column: "EquipReq_ID",
                principalTable: "Equipments",
                principalColumn: "Equip_ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
