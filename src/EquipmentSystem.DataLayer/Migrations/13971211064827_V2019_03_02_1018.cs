﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_03_02_1018 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserEquip_ID",
                table: "User_EquipmentReqs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_User_EquipmentReqs_UserEquip_ID",
                table: "User_EquipmentReqs",
                column: "UserEquip_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs",
                column: "UserEquip_ID",
                principalTable: "User_Equipments",
                principalColumn: "UserEquip_ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_User_Equipments_UserEquip_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropIndex(
                name: "IX_User_EquipmentReqs_UserEquip_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropColumn(
                name: "UserEquip_ID",
                table: "User_EquipmentReqs");
        }
    }
}
