﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_02_25_1252 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserReq_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropColumn(
                name: "AdminAction",
                table: "User_EquipmentReqs");

            migrationBuilder.RenameColumn(
                name: "UserAction",
                table: "User_EquipmentReqs",
                newName: "UserDo_ID");

            migrationBuilder.RenameColumn(
                name: "DateReq",
                table: "User_EquipmentReqs",
                newName: "DateReqAction");

            migrationBuilder.RenameColumn(
                name: "DateAct",
                table: "User_EquipmentReqs",
                newName: "DateDoAction");

            migrationBuilder.AddColumn<int>(
                name: "UserDoAction",
                table: "User_EquipmentReqs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserRecAction",
                table: "User_EquipmentReqs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_User_EquipmentReqs_UserDo_ID",
                table: "User_EquipmentReqs",
                column: "UserDo_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserDo_ID",
                table: "User_EquipmentReqs",
                column: "UserDo_ID",
                principalTable: "Users",
                principalColumn: "User_ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserReq_ID",
                table: "User_EquipmentReqs",
                column: "UserReq_ID",
                principalTable: "Users",
                principalColumn: "User_ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserDo_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserReq_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropIndex(
                name: "IX_User_EquipmentReqs_UserDo_ID",
                table: "User_EquipmentReqs");

            migrationBuilder.DropColumn(
                name: "UserDoAction",
                table: "User_EquipmentReqs");

            migrationBuilder.DropColumn(
                name: "UserRecAction",
                table: "User_EquipmentReqs");

            migrationBuilder.RenameColumn(
                name: "UserDo_ID",
                table: "User_EquipmentReqs",
                newName: "UserAction");

            migrationBuilder.RenameColumn(
                name: "DateReqAction",
                table: "User_EquipmentReqs",
                newName: "DateReq");

            migrationBuilder.RenameColumn(
                name: "DateDoAction",
                table: "User_EquipmentReqs",
                newName: "DateAct");

            migrationBuilder.AddColumn<int>(
                name: "AdminAction",
                table: "User_EquipmentReqs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_User_EquipmentReqs_Users_UserReq_ID",
                table: "User_EquipmentReqs",
                column: "UserReq_ID",
                principalTable: "Users",
                principalColumn: "User_ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
