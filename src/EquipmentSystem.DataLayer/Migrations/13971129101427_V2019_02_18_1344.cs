﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentSystem.DataLayer.Migrations
{
    public partial class V2019_02_18_1344 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User_EquipmentReqs",
                columns: table => new
                {
                    UserEquipReq_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SHSerialReq = table.Column<string>(maxLength: 255, nullable: false),
                    SHAmvalReq = table.Column<string>(maxLength: 255, nullable: false),
                    Desc1Req = table.Column<string>(maxLength: 255, nullable: true),
                    Desc2Req = table.Column<string>(maxLength: 255, nullable: true),
                    Desc3Req = table.Column<string>(maxLength: 255, nullable: true),
                    Desc4Req = table.Column<string>(maxLength: 255, nullable: true),
                    Desc5Req = table.Column<string>(maxLength: 255, nullable: true),
                    Desc6Req = table.Column<string>(maxLength: 255, nullable: true),
                    UserReq_ID = table.Column<int>(nullable: false),
                    EquipReq_ID = table.Column<int>(nullable: false),
                    DateReq = table.Column<DateTimeOffset>(nullable: false),
                    UserAction = table.Column<int>(nullable: false),
                    DateAct = table.Column<DateTimeOffset>(nullable: false),
                    AdminAction = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_EquipmentReqs", x => x.UserEquipReq_ID);
                    table.ForeignKey(
                        name: "FK_User_EquipmentReqs_Equipments_EquipReq_ID",
                        column: x => x.EquipReq_ID,
                        principalTable: "Equipments",
                        principalColumn: "Equip_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_EquipmentReqs_Users_UserReq_ID",
                        column: x => x.UserReq_ID,
                        principalTable: "Users",
                        principalColumn: "User_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_EquipmentReqs_EquipReq_ID",
                table: "User_EquipmentReqs",
                column: "EquipReq_ID");

            migrationBuilder.CreateIndex(
                name: "IX_User_EquipmentReqs_UserReq_ID",
                table: "User_EquipmentReqs",
                column: "UserReq_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User_EquipmentReqs");
        }
    }
}
