using EquipmentSystem.DomainClasses;
using Microsoft.EntityFrameworkCore;

namespace EquipmentSystem.DataLayer.Context {
    public class ApplicationDbContext : DbContext, IUnitOfWork {
        public ApplicationDbContext (DbContextOptions<ApplicationDbContext> options) : base (options) { }

        public virtual DbSet<User> Users { set; get; }
        public virtual DbSet<Role> Roles { set; get; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserToken> UserTokens { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Equipment> Equipments { get; set; }
        public virtual DbSet<User_Equipment> User_Equipments { get; set; }
        public virtual DbSet<User_EquipmentReq> User_EquipmentReqs { get; set; }

        protected override void OnModelCreating (ModelBuilder builder) {
            // it should be placed here, otherwise it will rewrite the following settings!
            base.OnModelCreating (builder);

            // Custom application mappings
            builder.Entity<User> (entity => {
                entity.Property (e => e.Username).HasMaxLength (450).IsRequired ();
                entity.HasIndex (e => e.Username).IsUnique ();
                entity.Property (e => e.Password).IsRequired ();
                entity.Property (e => e.SerialNumber).HasMaxLength (450);
            });

            builder.Entity<Role> (entity => {
                entity.Property (e => e.Role_Name).HasMaxLength (450).IsRequired ();
                entity.HasIndex (e => e.Role_Name).IsUnique ();
            });

            builder.Entity<UserRole> (entity => {
                entity.HasKey (e => new { e.User_ID, e.Role_ID });
                entity.HasIndex (e => e.User_ID);
                entity.HasIndex (e => e.Role_ID);
                entity.Property (e => e.User_ID);
                entity.Property (e => e.Role_ID);
                entity.HasOne (d => d.Role).WithMany (p => p.UserRoles).HasForeignKey (d => d.Role_ID);
                entity.HasOne (d => d.User).WithMany (p => p.UserRoles).HasForeignKey (d => d.User_ID);
            });

            builder.Entity<UserToken> (entity => {
                entity.HasOne (ut => ut.User)
                    .WithMany (u => u.UserTokens)
                    .HasForeignKey (ut => ut.User_ID);

                entity.Property (ut => ut.RefreshTokenIdHash).HasMaxLength (450).IsRequired ();
                entity.Property (ut => ut.RefreshTokenIdHashSource).HasMaxLength (450);
            });
            //-----------------------------------
            builder.Entity<User_EquipmentReq> (entity => {
                entity.HasOne (ut => ut.UserReq)
                    .WithMany (u => u.Req_User_EquipmentReqs)
                    .HasForeignKey (ut => ut.UserReq_ID).OnDelete (DeleteBehavior.Restrict);

            });

            builder.Entity<User_EquipmentReq> (entity => {
                entity.HasOne (ut => ut.UserDo)
                    .WithMany (u => u.Do_User_EquipmentReqs)
                    .HasForeignKey (ut => ut.UserDo_ID).OnDelete (DeleteBehavior.Restrict);;

            });
            //-----------------------------------
        }
    }
}