﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [Equipments] (
        [Equip_ID] int NOT NULL IDENTITY,
        [Equip_Name] nvarchar(255) NOT NULL,
        [Parent_Equip_ID] int NULL,
        CONSTRAINT [PK_Equipments] PRIMARY KEY ([Equip_ID]),
        CONSTRAINT [FK_Equipments_Equipments_Parent_Equip_ID] FOREIGN KEY ([Parent_Equip_ID]) REFERENCES [Equipments] ([Equip_ID]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [Offices] (
        [Office_ID] int NOT NULL IDENTITY,
        [Office_Name] nvarchar(255) NOT NULL,
        [Parent_Office_ID] int NULL,
        [Office_Code] nvarchar(255) NOT NULL,
        CONSTRAINT [PK_Offices] PRIMARY KEY ([Office_ID])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [Roles] (
        [Role_ID] int NOT NULL IDENTITY,
        [Role_Name] nvarchar(450) NOT NULL,
        [Role_Caption] nvarchar(255) NOT NULL,
        CONSTRAINT [PK_Roles] PRIMARY KEY ([Role_ID])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [Users] (
        [User_ID] int NOT NULL IDENTITY,
        [User_Fname] nvarchar(255) NOT NULL,
        [User_Lname] nvarchar(255) NOT NULL,
        [User_UserName] nvarchar(450) NOT NULL,
        [User_Password] nvarchar(255) NOT NULL,
        [IsActive] bit NOT NULL,
        [LastLoggedIn] datetimeoffset NOT NULL,
        [SerialNumber] nvarchar(450) NOT NULL,
        [Office_ID] int NOT NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([User_ID]),
        CONSTRAINT [FK_Users_Offices_Office_ID] FOREIGN KEY ([Office_ID]) REFERENCES [Offices] ([Office_ID]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [User_Equipments] (
        [UserEquip_ID] int NOT NULL IDENTITY,
        [SHSerial] nvarchar(255) NOT NULL,
        [SHAmval] nvarchar(255) NOT NULL,
        [Desc1] nvarchar(255) NULL,
        [Desc2] nvarchar(255) NULL,
        [Desc3] nvarchar(255) NULL,
        [Desc4] nvarchar(255) NULL,
        [Desc5] nvarchar(255) NULL,
        [Desc6] nvarchar(255) NULL,
        [User_ID] int NOT NULL,
        [Equip_ID] int NOT NULL,
        CONSTRAINT [PK_User_Equipments] PRIMARY KEY ([UserEquip_ID]),
        CONSTRAINT [FK_User_Equipments_Equipments_Equip_ID] FOREIGN KEY ([Equip_ID]) REFERENCES [Equipments] ([Equip_ID]) ON DELETE CASCADE,
        CONSTRAINT [FK_User_Equipments_Users_User_ID] FOREIGN KEY ([User_ID]) REFERENCES [Users] ([User_ID]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [UserRoles] (
        [User_ID] int NOT NULL,
        [Role_ID] int NOT NULL,
        CONSTRAINT [PK_UserRoles] PRIMARY KEY ([User_ID], [Role_ID]),
        CONSTRAINT [FK_UserRoles_Roles_Role_ID] FOREIGN KEY ([Role_ID]) REFERENCES [Roles] ([Role_ID]) ON DELETE CASCADE,
        CONSTRAINT [FK_UserRoles_Users_User_ID] FOREIGN KEY ([User_ID]) REFERENCES [Users] ([User_ID]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE TABLE [UserTokens] (
        [UserToken_ID] int NOT NULL IDENTITY,
        [AccessTokenHash] nvarchar(max) NULL,
        [AccessTokenExpiresDateTime] datetimeoffset NOT NULL,
        [RefreshTokenIdHash] nvarchar(450) NOT NULL,
        [RefreshTokenIdHashSource] nvarchar(450) NULL,
        [RefreshTokenExpiresDateTime] datetimeoffset NOT NULL,
        [User_ID] int NOT NULL,
        CONSTRAINT [PK_UserTokens] PRIMARY KEY ([UserToken_ID]),
        CONSTRAINT [FK_UserTokens_Users_User_ID] FOREIGN KEY ([User_ID]) REFERENCES [Users] ([User_ID]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_Equipments_Parent_Equip_ID] ON [Equipments] ([Parent_Equip_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE UNIQUE INDEX [IX_Roles_Role_Name] ON [Roles] ([Role_Name]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_User_Equipments_Equip_ID] ON [User_Equipments] ([Equip_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_User_Equipments_User_ID] ON [User_Equipments] ([User_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_UserRoles_Role_ID] ON [UserRoles] ([Role_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_UserRoles_User_ID] ON [UserRoles] ([User_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_Users_Office_ID] ON [Users] ([Office_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE UNIQUE INDEX [IX_Users_User_UserName] ON [Users] ([User_UserName]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    CREATE INDEX [IX_UserTokens_User_ID] ON [UserTokens] ([User_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115175024_V2019_02_04_2120')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971115175024_V2019_02_04_2120', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115202659_V2019_02_04_2356')
BEGIN
    EXEC sp_rename N'[Users].[User_UserName]', N'Username', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115202659_V2019_02_04_2356')
BEGIN
    EXEC sp_rename N'[Users].[User_Password]', N'Password', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115202659_V2019_02_04_2356')
BEGIN
    EXEC sp_rename N'[Users].[IX_Users_User_UserName]', N'IX_Users_Username', N'INDEX';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971115202659_V2019_02_04_2356')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971115202659_V2019_02_04_2356', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971116045331_V2019_02_05_0823')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971116045331_V2019_02_05_0823', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971119083202_V2019_02_08_1201')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'LastLoggedIn');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [Users] ALTER COLUMN [LastLoggedIn] datetimeoffset NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971119083202_V2019_02_08_1201')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971119083202_V2019_02_08_1201', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971119083539_V2019_02_08_1205')
BEGIN
    DECLARE @var1 sysname;
    SELECT @var1 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'SerialNumber');
    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var1 + '];');
    ALTER TABLE [Users] ALTER COLUMN [SerialNumber] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971119083539_V2019_02_08_1205')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971119083539_V2019_02_08_1205', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971129101427_V2019_02_18_1344')
BEGIN
    CREATE TABLE [User_EquipmentReqs] (
        [UserEquipReq_ID] int NOT NULL IDENTITY,
        [SHSerialReq] nvarchar(255) NOT NULL,
        [SHAmvalReq] nvarchar(255) NOT NULL,
        [Desc1Req] nvarchar(255) NULL,
        [Desc2Req] nvarchar(255) NULL,
        [Desc3Req] nvarchar(255) NULL,
        [Desc4Req] nvarchar(255) NULL,
        [Desc5Req] nvarchar(255) NULL,
        [Desc6Req] nvarchar(255) NULL,
        [UserReq_ID] int NOT NULL,
        [EquipReq_ID] int NOT NULL,
        [DateReq] datetimeoffset NOT NULL,
        [UserAction] int NOT NULL,
        [DateAct] datetimeoffset NOT NULL,
        [AdminAction] int NOT NULL,
        CONSTRAINT [PK_User_EquipmentReqs] PRIMARY KEY ([UserEquipReq_ID]),
        CONSTRAINT [FK_User_EquipmentReqs_Equipments_EquipReq_ID] FOREIGN KEY ([EquipReq_ID]) REFERENCES [Equipments] ([Equip_ID]) ON DELETE CASCADE,
        CONSTRAINT [FK_User_EquipmentReqs_Users_UserReq_ID] FOREIGN KEY ([UserReq_ID]) REFERENCES [Users] ([User_ID]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971129101427_V2019_02_18_1344')
BEGIN
    CREATE INDEX [IX_User_EquipmentReqs_EquipReq_ID] ON [User_EquipmentReqs] ([EquipReq_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971129101427_V2019_02_18_1344')
BEGIN
    CREATE INDEX [IX_User_EquipmentReqs_UserReq_ID] ON [User_EquipmentReqs] ([UserReq_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971129101427_V2019_02_18_1344')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971129101427_V2019_02_18_1344', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [FK_User_EquipmentReqs_Users_UserReq_ID];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    DECLARE @var2 sysname;
    SELECT @var2 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'AdminAction');
    IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var2 + '];');
    ALTER TABLE [User_EquipmentReqs] DROP COLUMN [AdminAction];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    EXEC sp_rename N'[User_EquipmentReqs].[UserAction]', N'UserDo_ID', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    EXEC sp_rename N'[User_EquipmentReqs].[DateReq]', N'DateReqAction', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    EXEC sp_rename N'[User_EquipmentReqs].[DateAct]', N'DateDoAction', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD [UserDoAction] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD [UserRecAction] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    CREATE INDEX [IX_User_EquipmentReqs_UserDo_ID] ON [User_EquipmentReqs] ([UserDo_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD CONSTRAINT [FK_User_EquipmentReqs_Users_UserDo_ID] FOREIGN KEY ([UserDo_ID]) REFERENCES [Users] ([User_ID]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD CONSTRAINT [FK_User_EquipmentReqs_Users_UserReq_ID] FOREIGN KEY ([UserReq_ID]) REFERENCES [Users] ([User_ID]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206092312_V2019_02_25_1252')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971206092312_V2019_02_25_1252', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [FK_User_EquipmentReqs_Equipments_EquipReq_ID];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var3 sysname;
    SELECT @var3 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'UserReq_ID');
    IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var3 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [UserReq_ID] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var4 sysname;
    SELECT @var4 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'UserRecAction');
    IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var4 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [UserRecAction] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var5 sysname;
    SELECT @var5 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'UserDo_ID');
    IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var5 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [UserDo_ID] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var6 sysname;
    SELECT @var6 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'UserDoAction');
    IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var6 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [UserDoAction] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var7 sysname;
    SELECT @var7 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'EquipReq_ID');
    IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var7 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [EquipReq_ID] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var8 sysname;
    SELECT @var8 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'DateReqAction');
    IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var8 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [DateReqAction] datetimeoffset NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    DECLARE @var9 sysname;
    SELECT @var9 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'DateDoAction');
    IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var9 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [DateDoAction] datetimeoffset NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD CONSTRAINT [FK_User_EquipmentReqs_Equipments_EquipReq_ID] FOREIGN KEY ([EquipReq_ID]) REFERENCES [Equipments] ([Equip_ID]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971206094944_V2019_02_25_1319')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971206094944_V2019_02_25_1319', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971211064827_V2019_03_02_1018')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD [UserEquip_ID] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971211064827_V2019_03_02_1018')
BEGIN
    CREATE INDEX [IX_User_EquipmentReqs_UserEquip_ID] ON [User_EquipmentReqs] ([UserEquip_ID]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971211064827_V2019_03_02_1018')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD CONSTRAINT [FK_User_EquipmentReqs_User_Equipments_UserEquip_ID] FOREIGN KEY ([UserEquip_ID]) REFERENCES [User_Equipments] ([UserEquip_ID]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971211064827_V2019_03_02_1018')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971211064827_V2019_03_02_1018', N'2.2.1-servicing-10028');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971213140318_V2019_03_04_1732')
BEGIN
    ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [FK_User_EquipmentReqs_User_Equipments_UserEquip_ID];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971213140318_V2019_03_04_1732')
BEGIN
    DECLARE @var10 sysname;
    SELECT @var10 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User_EquipmentReqs]') AND [c].[name] = N'UserEquip_ID');
    IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [User_EquipmentReqs] DROP CONSTRAINT [' + @var10 + '];');
    ALTER TABLE [User_EquipmentReqs] ALTER COLUMN [UserEquip_ID] int NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971213140318_V2019_03_04_1732')
BEGIN
    ALTER TABLE [User_EquipmentReqs] ADD CONSTRAINT [FK_User_EquipmentReqs_User_Equipments_UserEquip_ID] FOREIGN KEY ([UserEquip_ID]) REFERENCES [User_Equipments] ([UserEquip_ID]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'13971213140318_V2019_03_04_1732')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'13971213140318_V2019_03_04_1732', N'2.2.1-servicing-10028');
END;

GO

