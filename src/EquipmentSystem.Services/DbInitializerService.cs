using System;
using System.Linq;
using EquipmentSystem.Common;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EquipmentSystem.Services {
    public interface IDbInitializerService {
        /// <summary>
        /// Applies any pending migrations for the context to the database.
        /// Will create the database if it does not already exist.
        /// </summary>
        void Initialize ();

        /// <summary>
        /// Adds some default values to the Db
        /// </summary>
        void SeedData ();
    }

    public class DbInitializerService : IDbInitializerService {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ISecurityService _securityService;

        public DbInitializerService (
            IServiceScopeFactory scopeFactory,
            ISecurityService securityService) {
            _scopeFactory = scopeFactory;
            _scopeFactory.CheckArgumentIsNull (nameof (_scopeFactory));

            _securityService = securityService;
            _securityService.CheckArgumentIsNull (nameof (_securityService));
        }

        public void Initialize () {
            using (var serviceScope = _scopeFactory.CreateScope ()) {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext> ()) {
                    context.Database.Migrate ();
                }
            }
        }

        public void SeedData () {
            using (var serviceScope = _scopeFactory.CreateScope ()) {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext> ()) {
                    // Add default roles
                    var adminRole = new Role { Role_Name = CustomRoles.Admin, Role_Caption = "کاربر اصلی" };
                    var editorRole = new Role { Role_Name = CustomRoles.Editor, Role_Caption = "رئیس" };
                    var userRole = new Role { Role_Name = CustomRoles.User, Role_Caption = "کاربر" };
                    if (!context.Roles.Any ()) {
                        context.Add (adminRole);
                        context.Add (editorRole);
                        context.Add (userRole);
                        context.SaveChanges ();
                    }
                    // Add Admin user
                    if (!context.Users.Any ()) {
                        var adminUser = new User {
                            Username = "me",
                            User_Lname = "اخلاص",
                            User_Fname = "مهدی",
                            IsActive = true,
                            LastLoggedIn = DateTime.Now,
                            Password = _securityService.GetSha256Hash ("1234"),
                            SerialNumber = Guid.NewGuid ().ToString ("N"),
                            Office_ID = 21
                        };
                        context.Add (adminUser);
                        context.SaveChanges ();

                        context.Add (new UserRole { Role = adminRole, User = adminUser });
                        // context.Add (new UserRole { Role = userRole, User = adminUser });
                        context.SaveChanges ();
                    }
                }
            }
        }
    }
}