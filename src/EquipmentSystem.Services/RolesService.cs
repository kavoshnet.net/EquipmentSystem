using System.Linq;
using System.Collections.Generic;
using EquipmentSystem.Common;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace EquipmentSystem.Services
{
    public interface IRolesService
    {
        Task<List<Role>> FindUserRolesAsync(int userId);
        Task<bool> IsUserInRoleAsync(int userId, string roleName);
        Task<List<User>> FindUsersInRoleAsync(string roleName);
    }

    public class RolesService : IRolesService
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Role> _roles;
        private readonly DbSet<User> _users;

        public RolesService(IUnitOfWork uow)
        {
            _uow = uow;
            _uow.CheckArgumentIsNull(nameof(_uow));

            _roles = _uow.Set<Role>();
            _users = _uow.Set<User>();
        }

        public Task<List<Role>> FindUserRolesAsync(int userId)
        {
            var userRolesQuery = from role in _roles
                                 from userRoles in role.UserRoles
                                 where userRoles.User_ID == userId
                                 select role;

            return userRolesQuery.OrderBy(x => x.Role_Name).ToListAsync();
        }

        public async Task<bool> IsUserInRoleAsync(int userId, string roleName)
        {
            var userRolesQuery = from role in _roles
                                 where role.Role_Name == roleName
                                 from user in role.UserRoles
                                 where user.User_ID == userId
                                 select role;
            var userRole = await userRolesQuery.FirstOrDefaultAsync();
            return userRole != null;
        }

        public Task<List<User>> FindUsersInRoleAsync(string roleName)
        {
            var roleUserIdsQuery = from role in _roles
                                   where role.Role_Name == roleName
                                   from user in role.UserRoles
                                   select user.User_ID;
            return _users.Where(user => roleUserIdsQuery.Contains(user.User_ID))
                         .ToListAsync();
        }
    }
}