import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { AuthService } from "../../services/auth.service";
// import { UserService } from '../../../dashboard/Services/user.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy {
  title = "Angular.Jwt.Core";
  isAdmin = false;
  isUser = false;
  isEditor = false;
  result: any;

  isLoggedIn = false;
  subscription: Subscription | null = null;
  displayName = "";
  // officeName = ""
  // userId;
  // user:any;
  constructor(private authService: AuthService/*, private userService: UserService*/) { }

  ngOnInit() {
    this.subscription = this.authService.authStatus$.subscribe(status => {
      this.isLoggedIn = status;
      if (status) {
        const authUser = this.authService.getAuthUser();
        this.displayName = authUser ? authUser.displayName : "";
        this.isAdmin = this.authService.isAuthUserInRole('Admin');
        this.isUser = this.authService.isAuthUserInRole('User');
        this.isEditor = this.authService.isAuthUserInRole('Editor');
        // this.userId = authUser ? authUser.userId : undefined;
        // if (this.userId !== undefined) {
        //   debugger;
        //   this.userService.getUser(this.userId).subscribe((b:any) => {
        //     this.user = b;
        //   });
        //   this.officeName = this.user.office.Office_Name;
        // }
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  logout() {
    this.authService.logout(true);
  }
}
