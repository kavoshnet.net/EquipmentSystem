﻿import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken<string>('app.config');

export interface IAppConfig {
  apiEndpoint: string;
  apiSettingsPath: string;
}

export const AppConfig: IAppConfig = {
  // TODO: this linke will changed when compile and build project
  // apiEndpoint: 'http://ems.hm/api',
  apiEndpoint: 'http://localhost:5000/api',
  apiSettingsPath: 'ApiSettings'
};
