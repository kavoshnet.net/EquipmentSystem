import { Component, OnInit } from '@angular/core';
import {
  DataStateChangeEvent,
  GridDataResult
} from '@progress/kendo-angular-grid';
import { DataSourceRequestState } from '@progress/kendo-data-query';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators/switchMap';
import { GridUserService } from '../../Services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public loading: boolean=false;
  public state: DataSourceRequestState = {
    skip: 0,
    take: 20
  };
  public query: Observable<GridDataResult>;
  private stateChange = new BehaviorSubject<any>(this.state);

  // pagination configuration
  public buttonCount = 20;
  public info = true;
  public type: 'numeric' | 'input' = 'input';
  public pageSizes = [5, 10, 20, 40, 100];
  public previousNext = true;

  constructor(gridUserService: GridUserService) {
    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state => gridUserService.fetch(state)),
      tap(() => {
        this.loading = false;
      })
    );
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.stateChange.next(state);
  }

  ngOnInit() {
    // this.bookService.getBooks().subscribe(books => (this.books = books));
  }

}
