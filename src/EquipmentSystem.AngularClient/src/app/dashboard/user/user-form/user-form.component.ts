import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfficeService } from '../../Services/office.service';
import { UserService } from '../../Services/user.service';
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  user: any = {};
  offices: any ;
  constructor(
    _route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private officeServices: OfficeService
  ) {
    _route.params.subscribe(
      p => {
        this.user.user_ID = +p['id'];
      },
      err => {
        if (err.status === 404) {
          this.router.navigate(['/user']);
        }
      }
    );
  }

  ngOnInit() {
    if (this.user.user_ID) {
      this.userService.getUser(this.user.user_ID).subscribe(b => {
        this.user = b;
      });
    }
    this.officeServices.getOffices().subscribe((o:any) => {
      this.offices = o.data;
    });
  }
  submit() {
    if (this.user.user_ID) {
      this.userService.update(this.user).subscribe(() => {
        this.router.navigate(['/user']);
      });
    } else {
      this.user.user_ID = 0;
      this.user.lastLoggedIn = null;//Date.now();
      this.user.serialNumber = null;
      this.userService.create(this.user).subscribe(() => {
        this.router.navigate(['/user']);
      });
    }
  }
  delete() {
    if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.userService.delete(this.user.user_ID).subscribe(() => {
        this.router.navigate(['/user']);
      });
    }
  }
}
