import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { EquipmentService } from '../../Services/equipment.service';
@Component({
  selector: 'app-equipment-form',
  templateUrl: './equipment-form.component.html',
  styleUrls: ['./equipment-form.component.css']
})
export class EquipmentFormComponent implements OnInit {
  // tree config ---------------------------------------------------------------------------
  // tree config ---------------------------------------------------------------------------
  public selectedKeys: any[] = [];
  public checkedKeys: any[] = [];
  public expandedKeys: any[] = [];
  public selectkey = 'equip_ID';
  public checkkey = 'equip_ID';
  public expandkey = 'equip_ID';
  // public enableCheck = true;
  public enableCheck = false;
  public checkChildren = true;
  public checkParents = true;
  public checkMode: any = 'multiple';
  public selectionMode: any = 'single';
  public keys: string[] = [];
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: this.checkChildren,
      checkParents: this.checkParents,
      enabled: this.enableCheck,
      mode: this.checkMode
    };
  }

  // public data: any[] = [
  //   {
  //     text: 'Furniture', items: [
  //       { text: 'Tables & Chairs' },
  //       { text: 'Sofas' },
  //       {
  //         text: 'Occasional Furniture', items: [{
  //           text: 'Decor', items: [
  //             { text: 'Bed Linen' },
  //             { text: 'Curtains & Blinds' }
  //           ]
  //         }]
  //       }
  //     ]
  //   },
  //   { text: 'Decor' },
  //   { text: 'Outdoors' }
  // ];
  // public treeNodes: any[] = [
  //   {
  //     id: 1,
  //     parentId: null,
  //     desc: 'Root Node 1'
  //   }, {
  //     id: 2,
  //     parentId: null,
  //     desc: 'Root Node 2'
  //   }, {
  //     id: 3,
  //     parentId: 2,
  //     desc: 'Child node of Root Node 2'
  //   }, {
  //     id: 4,
  //     parentId: 3,
  //     desc: 'Child node of Root Node 2'
  //   }, {
  //     id: 5,
  //     parentId: 3,
  //     desc: 'Child node of Root Node 2'
  //   }
  // ];
  equipments: any[] = [];
  // tree config ---------------------------------------------------------------------------
  // tree config ---------------------------------------------------------------------------
  equipment: any = {};

  constructor(
    route: ActivatedRoute,
    private router: Router,
    private equipmentService: EquipmentService
  ) {
    route.params.subscribe(
      p => {
        this.equipment.equip_ID = +p['id'];
      },
      err => {
        if (err.status === 404) {
          this.router.navigate(['/equipment']);
        }
      }
    );
    this.equipmentService.getEquipments().subscribe((s: any) => {
      this.equipments = s.data;
    });
  }

  ngOnInit() {
    if (this.equipment.equip_ID) {
      this.equipmentService
        .getEquipment(this.equipment.equip_ID)
        .subscribe(b => {
          this.equipment = b;
          this.equipmentService
            .getEquipmentsParents(this.equipment.equip_ID)
            .subscribe(p => {
              this.selectedKeys.push(this.equipment.equip_ID);
              this.checkedKeys.push(this.equipment.equip_ID);
              this.expandedKeys = [p];
            });
        });
    }
  }
  submit() {
    if (this.equipment.equip_ID) {
      this.equipmentService.update(this.equipment).subscribe(() => {
        this.equipmentService.getEquipments().subscribe((s:any) => {
          this.equipments = s.data;
        });
        this.router.navigate(['/equipment/', this.equipment.equip_ID]);
      });
    } else {
      this.equipment.equip_ID = 0;
      this.equipmentService.create(this.equipment).subscribe(() => {
        this.equipmentService.getEquipments().subscribe((s:any) => {
          this.equipments = s.data;
        });
        this.router.navigate(['/equipment/', this.equipment.equip_ID]);
        // this.router.navigate(['/equipment']);
      });
    }
  }
  delete() {
    if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.equipmentService.delete(this.equipment.equip_ID).subscribe(() => {
        this.equipmentService.getEquipments().subscribe((s:any) => {
          this.equipments = s.data;
        });
        this.equipment.equip_ID = 0;
        this.router.navigate(['/equipment/', this.equipment.equip_ID]);
        // this.router.navigate(['/equipment']);
      });
    }
  }

  // tree config ---------------------------------------------------------------------------
  // tree config ---------------------------------------------------------------------------
  public handleCollapse(node:any) {
    // this.keys = this.keys.filter(k => k !== node.index);
    this.keys = this.keys.filter(k => k !== node.dataItem.equip_ID);
  }
  public handleExpand(node:any) {
    // this.keys = this.keys.concat(node.index);
    this.keys = this.keys.concat(node.dataItem.equip_ID);
  }

  public handleSelect(node:any) {
    // this.keys = this.keys.concat(node.index);
    // console.log(node);
    this.keys = this.keys.concat(node.dataItem.equip_ID);
    this.equipment.equip_ID = node.dataItem.equip_ID;
    this.equipment.equip_Name = node.dataItem.equip_Name;
    this.equipment.parent_Equip_ID = node.dataItem.parent_Equip_ID;
  }
  public expand() {
    this.expandedKeys.push(64);
  }
  // public children = (dataItem: any): any[] => of(dataItem.items);
  // public hasChildren = (dataItem: any): boolean => !!dataItem.items;

  // tree config ---------------------------------------------------------------------------
  // tree config ---------------------------------------------------------------------------
}
