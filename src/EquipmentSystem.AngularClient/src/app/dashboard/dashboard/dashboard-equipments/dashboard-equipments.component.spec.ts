import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEquipmentsComponent } from './dashboard-equipments.component';

describe('DashboardEquipmentsComponent', () => {
  let component: DashboardEquipmentsComponent;
  let fixture: ComponentFixture<DashboardEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
