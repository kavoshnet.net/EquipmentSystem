import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core';
import { SeriesLabels } from '@progress/kendo-angular-charts';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { DataSourceRequestState } from '@progress/kendo-data-query';
import { Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators/switchMap';
import { GridDashBoardUserEquipmentService } from '../../Services/user-equipment.service';
import { EquipmentService } from './../../Services/equipment.service';
import { OfficeService } from './../../Services/office.service';
import { UserService } from './../../Services/user.service';

interface Querystring {
  user_ID: any[];
  office_ID0: any[];
  office_ID1: any[];
  equipment_ID0: any[];
  equipment_ID1: any[];
  equipment_ID2: any[];
  clear: () => void;
}
interface Aggregate {
  Count: number;
  SumCount: number;
  // SumOfficeWage: number;
  // SumAddedValue: number;
}

@Component({
  selector: 'app-dashboard-equipments',
  templateUrl: './dashboard-equipments.component.html',
  styleUrls: ['./dashboard-equipments.component.css']
})
export class DashboardEquipmentsComponent implements OnInit {
  public chartType = 'bar';
  public seriesLabels: SeriesLabels = {
    visible: true, // Note that visible defaults to false
    padding: 3,
    font: '12px Vazir'
  };
  public loading: boolean = false;
  public state: DataSourceRequestState = {
    skip: 0,
    take: 0
  };
  public query: Observable<GridDataResult> | undefined;
  private stateChange = new BehaviorSubject<any>(this.state);

  // pagination configuration
  public buttonCount = 10;
  public info = true;
  public type: 'numeric' | 'input' = 'input';
  public pageSizes = [5, 10, 20, 40, 100];
  public previousNext = true;

  displayChild = false;

  // multi select basic configuration
  dropdownBaseSettings = {};
  // multi select basic configuration

  myval = {} as Aggregate;
  users: any[] = [];
  // multi select user configuration
  selectedUsers: any[] = [];
  dropdownUserSettings = new Object();
  // multi select user configuration

  offices0: any[] = [];
  // multi select office configuration
  selectedOffices0: any[] = [];
  dropdownOffice0Settings = new Object();
  // multi select office configuration

  offices1: any[] = [];
  // multi select office1 configuration
  selectedOffices1: any[] = [];
  dropdownOffice1Settings = new Object();
  // multi select office1 configuration

  equipments0: any[] = [];
  // multi select equipment0 configuration
  selectedEquipments0: any[] = [];
  dropdownEquipment0Settings = new Object();
  // multi select equipment0 configuration

  equipments1: any[] = [];
  // multi select equipment1 configuration
  selectedEquipments1: any[] = [];
  dropdownEquipment1Settings = new Object();
  // multi select equipment1 configuration

  equipments2: any[] = [];
  // multi select equipment2 configuration
  selectedEquipments2: any[] = [];
  dropdownEquipment2Settings = new Object();
  // multi select equipment2 configuration

  _id1: any[] = [];
  _id2: any[] = [];
  _offid1: any[] = [];

  _queryString: Querystring = {
    user_ID: [],
    office_ID0: [],
    office_ID1: [],
    equipment_ID0: [],
    equipment_ID1: [],
    equipment_ID2: [],
    clear: (): void => {
      this._queryString.user_ID = [];
      this._queryString.office_ID0 = [];
      this._queryString.office_ID1 = [];
      this._queryString.equipment_ID0 = [];
      this._queryString.equipment_ID1 = [];
      this._queryString.equipment_ID2 = [];
    }
  };

  isAdmin = false;
  isUser = false;
  isEditor = false;
  result: any;
  userId: any;
  isLoggedIn = false;
  subscription: Subscription | null = null;
  displayName = "";


  // Chart Section -------------------------------------------------------------
  // public pieData: any = [
  //   { equip_Name: 'Scaner Avision', count: 1 },
  //   { equip_Name: 'Scaner Avision 610', count: 6 },
  //   { equip_Name: 'Scaner HP 8250', count: 6 },
  //   { equip_Name: 'Scaner HP G4010', count: 45 },
  //   { equip_Name: 'Scaner KODAK I1420', count: 1 },
  //   { equip_Name: 'Scaner HP Scanjet 8200', count: 8 },
  //   { equip_Name: 'Scaner KODAK I1405', count: 3 },
  //   { equip_Name: 'Scaner KODAK i1220', count: 7 },
  //   { equip_Name: 'plustek', count: 1 },
  //   { equip_Name: 'اسکنر Plustec', count: 3 },
  //   { equip_Name: 'RealScan', count: 1 },
  //   { equip_Name: 'اسکنر Plustec 2600', count: 5 },
  //   { equip_Name: 'اسکنر OPIC PRO A320', count: 1 },
  //   { equip_Name: 'Scaner Avision 600', count: 2 }
  // ];
  // public colors = [
  //   'orang',
  //   'green',
  //   'yellow',
  //   'red',
  //   'blue',
  //   'yellow',
  //   'green'
  // ];
  // Chart Section -------------------------------------------------------------

  constructor(
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService,
    private equipmentService: EquipmentService,
    private gridDashBoardUserEquipmentService: GridDashBoardUserEquipmentService,
    private authService: AuthService
  ) { }

  myData: any;
  ngOnInit() {

    this.subscription = this.authService.authStatus$.subscribe(status => {
      this.isLoggedIn = status;
      if (status) {
        const authUser = this.authService.getAuthUser();
        this.displayName = authUser ? authUser.displayName : "";
        this.isAdmin = this.authService.isAuthUserInRole('Admin');
        this.isUser = this.authService.isAuthUserInRole('User');
        this.isEditor = this.authService.isAuthUserInRole('Editor');
        this.userId = authUser ? authUser.userId : undefined;
        // if (this.userId !== undefined) {
        //   debugger;
        //   this.userService.getUser(this.userId).subscribe((b:any) => {
        //     this.user = b;
        //   });
        //   this.officeName = this.user.office.Office_Name;
        // }
      }
    });



    // multi select basic configuration
    this.dropdownBaseSettings = {
      singleSelection: false,
      selectAllText: 'انتخاب همه',
      unSelectAllText: 'عدم انتخاب همه',
      searchPlaceholderText: 'جستجو',
      filterSelectAllText: 'انتخاب همه داده های فیلتر شده',
      filterUnSelectAllText: 'عدم انتخاب همه داده های فیلتر شده',
      noDataLabel: 'داده ای یافت نشد',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      searchAutofocus: true,
      // lazyLoading: true,
      classes: 'myclass custom-class'
    };
    // multi select basic configuration

    // multi select office0 configuration
    this.dropdownOffice0Settings = Object.assign({}, this.dropdownBaseSettings);

    // this.dropdownOffice0Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'office_Name',
    //   primaryKey:'office_ID'
    // }

    this.dropdownOffice0Settings['text'] = 'انتخاب اداره';
    this.dropdownOffice0Settings['labelKey'] = 'office_Name';
    this.dropdownOffice0Settings['primaryKey'] = 'office_ID';

    // multi select office0 configuration

    // multi select office configuration
    this.dropdownOffice1Settings = Object.assign({}, this.dropdownBaseSettings);

    // this.dropdownOffice1Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'office_Name',
    //   primaryKey:'office_ID'
    // }

    this.dropdownOffice1Settings['text'] = 'انتخاب اداره';
    this.dropdownOffice1Settings['labelKey'] = 'office_Name';
    this.dropdownOffice1Settings['primaryKey'] = 'office_ID';

    // multi select office configuration
    if (this.isAdmin) {
      this.officeService.getOfficesRoot().subscribe((s: any) => {
        this.offices0 = s.data;
      });
    } else if (this.isEditor) {
      this.officeService.getOfficeRootUserLoggedIn().subscribe((s: any) => {
        this.offices0 = s.data;
      });
    }
    else if (this.isUser) {
      this.offices0 = [];
    }

    // multi select user configuration
    this.dropdownUserSettings = Object.assign({}, this.dropdownBaseSettings);

    // this.dropdownUserSettings={
    //   text:'انتخاب اداره',
    //   labelKey:'user_Lname',
    //   primaryKey:'user_ID'
    // }

    this.dropdownUserSettings['text'] = 'انتخاب کاربر';
    this.dropdownUserSettings['labelKey'] = 'user_Lname';
    this.dropdownUserSettings['primaryKey'] = 'user_ID';

    // multi select user configuration

    // multi select equipment0 configuration
    this.dropdownEquipment0Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment0Settings={
    //   text:'انتخاب اداره',
    //   labelKey:['equip_Name'],
    //   primaryKey:'equip_ID'
    // }

    this.dropdownEquipment0Settings['text'] = 'انتخاب نوع تجهیزات';
    this.dropdownEquipment0Settings['labelKey'] = ['equip_Name'];
    this.dropdownEquipment0Settings['primaryKey'] = 'equip_ID';

    // multi select equipment0 configuration

    // multi select equipment1 configuration
    this.dropdownEquipment1Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment1Settings={
    //   text:'انتخاب اداره',
    //   labelKey:['equip_Name'],
    //   primaryKey:'equip_ID'
    // }


    this.dropdownEquipment1Settings['text'] = 'انتخاب نوع برند';
    this.dropdownEquipment1Settings['labelKey'] = ['equip_Name'];
    this.dropdownEquipment1Settings['primaryKey'] = 'equip_ID';

    // multi select equipment1 configuration

    // multi select equipment2 configuration
    this.dropdownEquipment2Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment2Settings={
    //   text:'انتخاب اداره',
    //   labelKey:['equip_Name'],
    //   primaryKey:'equip_ID'
    // }

    this.dropdownEquipment2Settings['text'] = 'انتخاب نوع مدل';
    this.dropdownEquipment2Settings['labelKey'] = ['equip_Name'];
    this.dropdownEquipment2Settings['primaryKey'] = 'equip_ID';

    // multi select equipment2 configuration

    this.getUsers();

    this.equipmentService.getEquipmentsRoot().subscribe((s: any) => {
      this.equipments0 = s.data;
    });
    this._id1 = [];
    this._id2 = [];
    this._offid1 = [];
  }

  searchUserEquipment() {
    // multi select basic configuration
    this.mapQueryString();
    // multi select basic configuration

    this.myData = 'user_ID=' + this._queryString.user_ID + '&office_ID=';
    if (this._queryString.office_ID1.length !== 0) {
      this.myData += this._queryString.office_ID1;
    } else if (this._queryString.office_ID0.length !== 0) {
      this.myData += this._queryString.office_ID0;
    }
    this.myData += '&equipment_ID=';
    if (this._queryString.equipment_ID2.length !== 0) {
      this.myData += this._queryString.equipment_ID2;
    } else if (this._queryString.equipment_ID1.length !== 0) {
      this.myData += this._queryString.equipment_ID1;
    } else if (this._queryString.equipment_ID0.length !== 0) {
      this.myData += this._queryString.equipment_ID0;
    }

    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state =>
        this.gridDashBoardUserEquipmentService.fetch(state, this.myData)
      ),
      tap<any>(agg => {
        this.loading = false;
        const queryParams: any = {};
        if (this._queryString.user_ID) {
          queryParams.user_ID = this._queryString.user_ID;
        }
        if (this._queryString.office_ID1.length !== 0) {
          queryParams.office_ID = this._queryString.office_ID1;
        } else if (this._queryString.office_ID0.length !== 0) {
          queryParams.office_ID = this._queryString.office_ID0;
        }
        if (this._queryString.equipment_ID2.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID2;
        } else if (this._queryString.equipment_ID1.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID1;
        } else if (this._queryString.equipment_ID0.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID0;
        }
        this.router.navigate(['/dashboard/search'], {
          queryParams: queryParams
        });
        // this.router.navigate(['/book']);
        this.myval.Count = agg.count;
        this.myval.SumCount = agg.sumCount;
      })
    );
  }
  onChangeOF0(_items: any) {
    this.mapQueryString();

    this.offices1 = [];
    // multi select basic configuration
    this.selectedOffices1 = [];
    // multi select basic configuration
    this._queryString.office_ID1 = [];
    this._offid1 = [];
    this.getUsers();
  }
  onChangeOF1(_items: any) {
    this.mapQueryString();
    this.offices1 = [];
    // multi select basic configuration

    // multi select basic configuration
    this.selectedOffices1 = [];
    this._offid1 = this._queryString.office_ID0;
    if (this._offid1.length > 0) {
      this.officeService.getOfficesChild(this._offid1).subscribe((s: any) => {
        this.offices1 = s.data;
      });
    }
    this.getUsers();
    this.searchUserEquipment();
  }

  onChangeEQ0(_items: any) {
    this.mapQueryString();
    this.equipments1 = [];
    // multi select basic configuration
    this.selectedEquipments1 = [];
    this.equipments2 = [];
    this.selectedEquipments2 = [];
    // multi select basic configuration
    this._queryString.equipment_ID1 = [];
    this._queryString.equipment_ID2 = [];
    this._id1 = [];
    this._id2 = [];
  }
  onChangeEQ1(_items: any) {
    this.mapQueryString();
    this.equipments2 = [];
    // multi select basic configuration
    this.selectedEquipments2 = [];
    this._queryString.equipment_ID2 = [];
    this._id2 = [];

    this.equipments1 = [];
    // multi select basic configuration
    this.selectedEquipments1 = [];
    this._queryString.equipment_ID1 = [];

    this._id1 = this._queryString.equipment_ID0;
    if (this._id1.length > 0) {
      this.equipmentService.getEquipmentsChild(this._id1).subscribe((s: any) => {
        this.equipments1 = s.data;
      });
    }
    this.searchUserEquipment();
  }
  onChangeEQ2(_items: any) {
    // multi select basic configuration
    this.mapQueryString();
    this.equipments2 = [];
    // multi select basic configuration
    this.selectedEquipments2 = [];
    this._queryString.equipment_ID2 = [];

    this._id2 = this._queryString.equipment_ID1;
    if (this._id2.length > 0) {
      this.equipmentService.getEquipmentsChild(this._id2).subscribe((s: any) => {
        this.equipments2 = s.data;
      });
    }
    this.searchUserEquipment();
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.stateChange.next(state);
  }
  getUsers() {
    // multi select basic configuration
    this.mapQueryString();
    if (
      !this._queryString.office_ID0 ||
      this._queryString.office_ID0.length === 0
    ) {
      // multi select basic configuration
      this.selectedUsers = [];
      this._queryString.user_ID = [];
      if (this.isAdmin) {
        this.userService.getUsers().subscribe((u: any) => {
          this.users = u.data;
        });
      } else if (this.isEditor) {
        this.userService.getUserOf_Office_UserLoggedIn().subscribe((u: any) => {
          this.users = u.data;
        });

      }
      else if (this.isUser) {
        this.users = [];
      }

    } else if (
      !this._queryString.office_ID1 ||
      this._queryString.office_ID1.length === 0
    ) {
      this.userService
        .getUsersOffices(this._queryString.office_ID0)
        .subscribe((u: any) => {
          this.users = u.data;
        });
    } else {
      this.userService
        .getUsersOffices(this._queryString.office_ID1)
        .subscribe((u: any) => {
          this.users = u.data;
        });
    }
  }
  // multi select basic configuration
  mapQueryString() {
    this._queryString.office_ID0 = this.selectedOffices0
      ? this.selectedOffices0.map(t => t.office_ID)
      : [];

    this._queryString.office_ID1 = this.selectedOffices1
      ? this.selectedOffices1.map(t => t.office_ID)
      : [];

    this._queryString.user_ID = this.selectedUsers
      ? this.selectedUsers.map(t => t.user_ID)
      : [];

    this._queryString.equipment_ID0 = this.selectedEquipments0
      ? this.selectedEquipments0.map(t => t.equip_ID)
      : [];

    this._queryString.equipment_ID1 = this.selectedEquipments1
      ? this.selectedEquipments1.map(t => t.equip_ID)
      : [];

    this._queryString.equipment_ID2 = this.selectedEquipments2
      ? this.selectedEquipments2.map(t => t.equip_ID)
      : [];
  }

  onSeriesClick($event) {
    // console.log($event);
    // console.log($event.category);
    // console.log($event.dataItem.equip_ID);
    if (this.equipments2.length > 0 && this.selectedEquipments2.length === 0) {
      var result = this.equipments2.find(o => o.equip_ID === $event.dataItem.equip_ID);
      this._queryString.equipment_ID2 = $event.dataItem.equip_ID;
      this.selectedEquipments2.push(result);

      this._id2.push(this._queryString.equipment_ID2);

      this.searchUserEquipment();
    } else if (this.equipments1.length > 0 && this.selectedEquipments1.length === 0) {
      var result = this.equipments1.find(o => o.equip_ID === $event.dataItem.equip_ID);
      this._queryString.equipment_ID1 = $event.dataItem.equip_ID;
      this.selectedEquipments1.push(result);

      this.equipments2 = [];
      // multi select basic configuration
      this.selectedEquipments2 = [];
      this._queryString.equipment_ID2 = [];
      this._id2 = [];
      this._id2.push(this._queryString.equipment_ID1);

      // this._id2 = this._queryString.equipment_ID1;
      if (this._id2.length > 0) {
        this.equipmentService.getEquipmentsChild(this._id2).subscribe((s: any) => {
          this.equipments2 = s.data;
        });
      }
      this.searchUserEquipment();

    } else if (this.equipments0.length > 0 && this.selectedEquipments0.length === 0) {
      var result = this.equipments0.find(o => o.equip_ID === $event.dataItem.equip_ID);
      this._queryString.equipment_ID0 = $event.dataItem.equip_ID;
      this.selectedEquipments0.push(result);
      this.equipments2 = [];
      // multi select basic configuration
      this.selectedEquipments2 = [];
      this._queryString.equipment_ID2 = [];
      this._id2 = [];

      this.equipments1 = [];
      // multi select basic configuration
      this.selectedEquipments1 = [];
      this._queryString.equipment_ID1 = [];
      this._id1 = [];
      this._id1.push(this._queryString.equipment_ID0);
      if (this._id1.length > 0) {
        this.equipmentService.getEquipmentsChild(this._id1).subscribe((s: any) => {
          this.equipments1 = s.data;
        });
      }
      this.searchUserEquipment();

    }
  }
  onClickBack() {
    if (this.equipments2.length > 0) {
      this.selectedEquipments2 = [];
      this.selectedEquipments1 = [];
      this.equipments2 = [];

      // this._queryString.equipment_ID2 = [];
      this._id2 = [];
      this.searchUserEquipment();
    } else if (this.equipments1.length > 0) {
      this.selectedEquipments2 = [];
      this.selectedEquipments1=[]
      this.selectedEquipments0=[]
      this.equipments2 = [];
      this.equipments1 = [];

      // this._queryString.equipment_ID2 = [];
      // this._queryString.equipment_ID1 = [];
      this._id2 = [];
      this._id1 = [];
      this.searchUserEquipment();
    } else if (this.equipments0.length > 0 /*&& this.selectedEquipments0.length*/) {
      this.selectedEquipments2 = [];
      this.selectedEquipments1 = []
      this.selectedEquipments0 = []
      this.equipments2 = [];
      this.equipments1 = [];

      // this._queryString.equipment_ID2 = [];
      // this._queryString.equipment_ID1 = [];
      // this._queryString.equipment_ID0 = [];
      this._id2 = [];
      this._id1 = [];
      this.searchUserEquipment();

    }
  }
}
