import { Component, OnInit } from '@angular/core';
import {
  DataStateChangeEvent,
  GridDataResult
} from '@progress/kendo-angular-grid';
import { DataSourceRequestState } from '@progress/kendo-data-query';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { tap ,  switchMap } from 'rxjs/operators';
import { GridOfficeService } from '../../Services/office.service';
@Component({
  selector: 'app-office-list',
  templateUrl: './office-list.component.html',
  styleUrls: ['./office-list.component.css']
})
export class OfficeListComponent implements OnInit {
  public loading: boolean=false;
  public state: DataSourceRequestState = {
    skip: 0,
    take: 20
  };
  public query: Observable<GridDataResult>;
  private stateChange = new BehaviorSubject<any>(this.state);

  // pagination configuration
  public buttonCount = 20;
  public info = true;
  public type: 'numeric' | 'input' = 'input';
  public pageSizes = [5, 10, 20, 40, 100];
  public previousNext = true;

  constructor(gridOfficeService: GridOfficeService) {
    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state => gridOfficeService.fetch(state)),
      tap(() => {
        this.loading = false;
      })
    );
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.stateChange.next(state);
  }

  ngOnInit() {
    // this.bookService.getBooks().subscribe(books => (this.books = books));
  }

}
