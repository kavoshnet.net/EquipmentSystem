import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfficeService } from '../../Services/office.service';

@Component({
  selector: 'app-office-form',
  templateUrl: './office-form.component.html',
  styleUrls: ['./office-form.component.css']
})
export class OfficeFormComponent implements OnInit {
  office: any = {};
  offices: any;

  constructor(
    _route: ActivatedRoute,
    private router: Router,
    private officeService: OfficeService
  ) {
    _route.params.subscribe(
      p => {
        this.office.office_ID = +p['id'];
      },
      err => {
        if (err.status === 404) {
          this.router.navigate(['/office']);
        }
      }
    );
  }

  ngOnInit() {
    if (this.office.office_ID) {
      this.officeService.getOffice(this.office.office_ID).subscribe(b => {
        this.office = b;
      });
    }
    this.officeService.getOffices().subscribe((o: any) => {
      this.offices = o.data;
    });
  }
  submit() {
    if (this.office.office_ID) {
      this.officeService.update(this.office).subscribe(() => {
        this.router.navigate(['/office']);
      });
    } else {
      this.office.office_ID = 0;
      this.officeService.create(this.office).subscribe(
        () => { this.router.navigate(['/office']); }
      );
    }
  }
  delete() {
    if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.officeService.delete(this.office.office_ID).subscribe(() => {
        this.router.navigate(['/office']);
      });
    }
  }
  donull(){
    this.office.parent_Office_ID = 0;
    this.office.parent_Office_ID = null;
  }
}
