import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, AuthGuardPermission } from '@app/core';
import { CallProtectedApiComponent } from './call-protected-api/call-protected-api.component';
import { DashboardEquipmentsComponent } from './dashboard/dashboard-equipments/dashboard-equipments.component';
import { EquipmentFormComponent } from './equipment/equipment-form/equipment-form.component';
import { EquipmentListComponent } from './equipment/equipment-list/equipment-list.component';
import { OfficeFormComponent } from './office/office-form/office-form.component';
import { OfficeListComponent } from './office/office-list/office-list.component';
import { ProtectedPageComponent } from './protected-page/protected-page.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserEquipmentFormComponent } from './user_equipment/user-equipment-form/user-equipment-form.component';
import { UserEquipmentListComponent } from './user_equipment/user-equipment-list/user-equipment-list.component';
import { UserEquipmentSearchComponent } from './user_equipment/user-equipment-search/user-equipment-search.component';
import { UserEquReqDetailsComponent } from './user_equ_req/user-equ-req-details/user-equ-req-details.component';
import { UserEquReqFormComponent } from './user_equ_req/user-equ-req-form/user-equ-req-form.component';
const routes: Routes = [
  {
    path: 'protectedPage',
    component: ProtectedPageComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'callProtectedApi',
    component: CallProtectedApiComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'office',
    component: OfficeListComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'office/new',
    component: OfficeFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'office/:id',
    component: OfficeFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user',
    component: UserListComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user/new',
    component: UserFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user/:id',
    component: UserFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'equipment',
    component: EquipmentListComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'equipment/new',
    component: EquipmentFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'equipment/:id',
    component: EquipmentFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipment/new',
    component: UserEquipmentFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipment/search',
    component: UserEquipmentSearchComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipment/:id',
    component: UserEquipmentFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipment',
    component: UserEquipmentListComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard/search',
    component: DashboardEquipmentsComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipmentreq/new',
    component: UserEquReqFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipmentreq/edit/:id',
    component: UserEquReqFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipmentreq/delete/:id',
    component: UserEquReqFormComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'user_equipmentreq/reqdetail/:id',
    component: UserEquReqDetailsComponent,
    data: {
      permission: {
        permittedRoles: ['Admin', 'User', 'Editor']
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  // entryComponents: [UserEquipmentListComponent],
})
export class DashboardRoutingModule { }
