import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'grdfilter'
})
export class GrdfilterPipe implements PipeTransform {
  transform(items: any, filter: any, defaultFilter: boolean): any {
    // Compiler error
    var True: boolean = true;
    function escapeRegExp(string) {
      if (string !== undefined) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
      }
    }
    if (!filter) {
      return items;
    }

    if (!Array.isArray(items)) {
      return items;
    }

    if (filter && Array.isArray(items)) {
      const filterKeys = Object.keys(filter);

      if (defaultFilter) {
        return items.filter(item =>
          filterKeys.reduce(
            (x, keyName) =>
              (x &&
                new RegExp(escapeRegExp(filter[keyName]), 'gi').test(
                  item[keyName]
                )) ||
              filter[keyName] === '',
            // Compiler error
            //true
            True)
        );
      } else {
        return items.filter(item => {
          return filterKeys.some(keyName => {
            return (
              new RegExp(escapeRegExp(filter[keyName]), 'gi').test(
                item[keyName]
              ) || filter[keyName] === ''
            );
          });
        });
      }
    }
  }
}
