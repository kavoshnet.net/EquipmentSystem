import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { FilterService } from '@progress/kendo-angular-grid';
import { CompositeFilterDescriptor, distinct, filterBy } from '@progress/kendo-data-query';

@Component({
  selector: 'app-multicheck-filter',
  templateUrl: './multicheck-filter.component.html',
  styleUrls: ['./multicheck-filter.component.css']
})
export class MulticheckFilterComponent implements AfterViewInit {
  @Input()
  public isPrimitive: boolean = false;
  @Input()
  public currentFilter!: CompositeFilterDescriptor;
  @Input()
  public data: any;
  @Input()
  public textField: any;
  @Input()
  public valueField: any;;
  @Input()
  public filterService!: FilterService;
  @Input()
  public field: string = '';
  @Output()
  public valueChange = new EventEmitter<number[]>();

  public currentData: any;
  public showFilter = true;
  private value: any[] = [];

  // Compiler Error
  // protected textAccessor = (dataItem: any) =>
  //   this.isPrimitive ? dataItem : dataItem[this.textField];
  public textAccessor = (dataItem: any) =>
    this.isPrimitive ? dataItem : dataItem[this.textField];
  // Compiler Error
  // protected valueAccessor = (dataItem: any) =>
  // this.isPrimitive ? dataItem : dataItem[this.valueField];
  public valueAccessor = (dataItem: any) =>
    this.isPrimitive ? dataItem : dataItem[this.valueField];

  public ngAfterViewInit() {
    this.currentData = this.data;
    this.value = this.currentFilter.filters.map(
      // (f: FilterDescriptor) => f.value
      (f: any) => f.value
    );
    this.showFilter =
      typeof this.textAccessor(this.currentData[0]) === 'string';
  }

  public isItemSelected(item: any) {
    return this.value.some(x => x === this.valueAccessor(item));
  }

  public onSelectionChange(item: any) {
    if (this.value.some(x => x === item)) {
      this.value = this.value.filter(x => x !== item);
    } else {
      this.value.push(item);
    }

    this.filterService.filter({
      filters: this.value.map(value => ({
        field: this.field,
        operator: 'eq',
        value
      })),
      logic: 'or'
    });
  }

  public onInput(e: any) {
    this.currentData = distinct(
      [
        ...this.currentData.filter((dataItem: any) =>
          this.value.some(val => val === this.valueAccessor(dataItem))
        ),
        ...filterBy(this.data, {
          operator: 'contains',
          field: this.textField,
          value: e.target.value
        })
      ],
      this.textField
    );
  }
}
