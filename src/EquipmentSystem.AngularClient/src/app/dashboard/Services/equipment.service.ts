import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, IAppConfig } from '@app/core';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {
  DataSourceRequestState,
  toDataSourceRequestString,
  translateDataSourceResultGroups
} from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
export abstract class ACEquipmentService {
  private BASE_URL = `/equipment`;
  public loading: boolean = false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
  ) { }

  public fetch(state: DataSourceRequestState): Observable<GridDataResult> {
    const queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response: any) =>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(Response['data'])
                : Response['data'],
              total: Response['total']
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class GridEquipmentService extends ACEquipmentService {
  constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, 'equipment', `${appConfig.apiEndpoint}`);
  }
}
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class EquipmentService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: IAppConfig,
    private http: HttpClient) { }
  create(equipment: any) {
    return this.http
      .post(`${this.appConfig.apiEndpoint}`+'/equipment', equipment);
  }
  getEquipment(id: any) {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/equipment/' + id).pipe(map(res => res || {}));
  }
  update(equipment: any) {
    return this.http
      .put(`${this.appConfig.apiEndpoint}`+'/equipment/' + equipment.equip_ID, equipment)
      .pipe(map(res => res || {}));
  }
  delete(id: any) {
    return this.http
      .delete(`${this.appConfig.apiEndpoint}`+'/equipment/' + id)
      .pipe(map(res => res || {}));
  }
  getEquipments() {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/equipment').pipe(map(res => res || {}));
  }
  // ------------------------------------------------------------------------------
  getEquipmentsRoot() {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/equipment/root').pipe(map(res => res || {}));
  }

  getEquipmentsParents(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}`+'/equipment/parents/' + id)
      .pipe(map(res => res || {}));
  }

  getEquipmentsChild(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}`+'/equipment/child/' + id)
      .pipe(map(res => res || {}));
  }

  // searchEquipment(myData: string) {
  //   // return this.http.get('/api/book').pipe(map(res => res.json()));
  //   // const data = { term: '2' };
  //   // return this.http.get('/api/book/byname' + myData);
  //   return this.http
  //     .get('/api/equipment/search', { params: myData })
  //     .pipe(map(res => res.json()));
  //   // return this.http.get('/api/book/searchbook', { params: data });

  //   // .pipe(map(res => res.json()));
  // }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
