import { TestBed } from '@angular/core/testing';

import { UserEquipmentreqService } from './user-equipmentreq.service';

describe('UserEquipmentreqService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserEquipmentreqService = TestBed.get(UserEquipmentreqService);
    expect(service).toBeTruthy();
  });
});
