import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { APP_CONFIG, IAppConfig } from "@app/core";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { DataSourceRequestState, toDataSourceRequestString, translateDataSourceResultGroups } from "@progress/kendo-data-query";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ----------------------------------ACUserEquipmentService-----------------
export abstract class ACUserEquipmentReqService {
  private BASE_URL = `/user_equipmentreq`;
  public loading: boolean = false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
  ) {}

  public fetch(
    state: DataSourceRequestState,
    _querystring: string
  ): Observable<GridDataResult> {
    let queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    if (_querystring !== "") {
      queryStr = queryStr + "&" + _querystring;
    }
    this.loading = true;
    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response: any) =>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(Response["data"])
                : Response["data"],
              total: Response["total"]
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// --------------------------------GridUserEquipmentService--------------------------------
@Injectable({ providedIn: "root" })
export class GridUserEquipmentReqService extends ACUserEquipmentReqService {
  constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, "user_equipmentreq", `${appConfig.apiEndpoint}`);
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

@Injectable({
  providedIn: "root"
})
export class UserEquipmentreqService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: IAppConfig,
    private http: HttpClient
  ) {}
  create(user_equipmentReq: any) {
    return this.http
      .post(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq",
        user_equipmentReq
      )
      .pipe(map(res => res || {}));
  }
  getUserEquipmentReq(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}` + "/user_equipmentreq/" + id)
      .pipe(map(res => res || {}));
  }
  getUserEquipmentReqReq(id: any) {
    return this.http
      .get(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq/reqdetail/" + id
      )
      .pipe(map(res => res || {}));
  }
  update(user_equipmentReq: any) {
    return this.http
      .put(
        `${this.appConfig.apiEndpoint}` +
          "/user_equipmentreq/" +
          user_equipmentReq.userEquip_ID,
        user_equipmentReq
      )
      .pipe(map(res => res || {}));
  }
  delete(id: any) {
    return this.http
      .delete(`${this.appConfig.apiEndpoint}` + "/user_equipmentreq/" + id)
      .pipe(map(res => res || {}));
  }
  getUserEquipmentReqs() {
    return this.http
      .get(`${this.appConfig.apiEndpoint}` + "/user_equipmentreq")
      .pipe(map(res => res || {}));
  }

  insertReq(user_equipmentReq: any) {
    return this.http
      .post(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq/reqdetail",
        user_equipmentReq
      )
      .pipe(map(res => res || {}));
  }

  updateReq(user_equipmentReq: any) {
    return this.http
      .put(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq/reqdetail",
        user_equipmentReq
      )
      .pipe(map(res => res || {}));
  }
  deleteReq(id: any) {
    return this.http
      .delete(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq/reqdetail/" + id
      )
      .pipe(map(res => res || {}));
  }
  rejectReq(user_equipmentReq: any) {
    return this.http
      .put(
        `${this.appConfig.apiEndpoint}` + "/user_equipmentreq/reqdetail/?state=reject",
        user_equipmentReq
      )
      .pipe(map(res => res || {}));
  }
}
