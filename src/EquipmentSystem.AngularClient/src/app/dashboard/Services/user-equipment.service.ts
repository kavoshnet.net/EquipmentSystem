import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, IAppConfig } from '@app/core';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { DataSourceRequestState, toDataSourceRequestString, translateDataSourceResultGroups } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ----------------------------------ACUserEquipmentService-----------------
export abstract class ACUserEquipmentService {
  private BASE_URL = `/user_equipment`;
  public loading: boolean=false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
    ) {}

  public fetch(state: DataSourceRequestState,
    _querystring: string): Observable<GridDataResult> {
    let queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    if (_querystring !== '') {
      queryStr = queryStr + '&' + _querystring;
    }    this.loading = true;
    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response :any)=>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(Response['data'])
                : Response['data'],
              total: Response['total']
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// --------------------------------GridUserEquipmentService--------------------------------
@Injectable({ providedIn: 'root' })
export class GridUserEquipmentService extends ACUserEquipmentService {
  constructor(http: HttpClient,
    @Inject(APP_CONFIG) appConfig: IAppConfig    ) {
    super(http, 'user_equipment',`${appConfig.apiEndpoint}`);
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------



// ---------------------------ACSearchUserEquipmentService----------------------------------
export abstract class ACSearchUserEquipmentService {
  private BASE_URL = `/user_equipment/search`;
  public loading: boolean=false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
    ) {}

  public fetch(
    state: DataSourceRequestState,
    _querystring: string
  ): Observable<GridDataResult> {
    let queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    if (_querystring !== '') {
      queryStr = queryStr + '&' + _querystring;
    }

    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response :any)=>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(
                    Response['user_equipments']['data']
                  )
                : Response['user_equipments']['data'],
              total: Response['user_equipments']['total'],
              // sumAmount: Response['sumAmount'],
              // sumOfficeWage: Response['sumOfficeWage'],
              // sumAddedValue: Response['sumAddedValue'],
              count: Response['count']
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// ---------------------------GridSearchUserEquipmentService----------------------------------
@Injectable({ providedIn: 'root' })
export class GridSearchUserEquipmentService extends ACSearchUserEquipmentService {
  constructor(http: HttpClient,@Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, 'user_equipment/search',`${appConfig.apiEndpoint}`);
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ---------------------------ACDashBoardUserEquipmentService----------------------------------
export abstract class ACDashBoardUserEquipmentService {
  private BASE_URL = `/user_equipment/dashsearch`;
  public loading: boolean=false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
    ) {}

  public fetch(
    state: DataSourceRequestState,
    _querystring: string
  ): Observable<GridDataResult> {
    let queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    if (_querystring !== '') {
      queryStr = queryStr + '&' + _querystring;
    }

    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response :any)=>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(
                    Response['user_equipments']['data']
                  )
                : Response['user_equipments']['data'],
              total: Response['user_equipments']['total'],
              sumCount: Response['sumCount'],
              // sumOfficeWage: Response['sumOfficeWage'],
              // sumAddedValue: Response['sumAddedValue'],
              count: Response['count']
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// ---------------------------GridDashBoardUserEquipmentService----------------------------------
@Injectable({ providedIn: 'root' })
export class GridDashBoardUserEquipmentService extends ACDashBoardUserEquipmentService {
  constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, 'user_equipment/dashsearch', `${appConfig.apiEndpoint}`);
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------












// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// -----------------------------------------------------------------------------------------

// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
@Injectable({ providedIn: 'root' })
export class UserEquipmentService {
  constructor( @Inject(APP_CONFIG) private appConfig: IAppConfig,private http: HttpClient) {}
  create(user_equipment: any) {
    return this.http
      .post(`${this.appConfig.apiEndpoint}`+'/user_equipment', user_equipment)
      .pipe(map(res => res || {}));
  }
  getUserEquipment(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}`+'/user_equipment/' + id)
      .pipe(map(res => res || {}));
  }
  update(user_equipment: any) {
    return this.http
      .put(`${this.appConfig.apiEndpoint}`+'/user_equipment/' + user_equipment.userEquip_ID, user_equipment)
      .pipe(map(res => res || {}));
  }
  delete(id: any) {
    return this.http
      .delete(`${this.appConfig.apiEndpoint}`+'/user_equipment/' + id)
      .pipe(map(res => res || {}));
  }
  getUserEquipments() {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/user_equipment').pipe(map(res => res || {}));
  }

  // searchOffice(myData: string) {
  //   // return this.http.get('/api/book').pipe(map(res => res.json()));
  //   // const data = { term: '2' };
  //   // return this.http.get('/api/book/byname' + myData);
  //   return this.http
  //     .get('/api/office/search', { params: myData })
  //     .pipe(map(res => res.json()));
  //   // return this.http.get('/api/book/searchbook', { params: data });

  //   // .pipe(map(res => res.json()));
  // }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
