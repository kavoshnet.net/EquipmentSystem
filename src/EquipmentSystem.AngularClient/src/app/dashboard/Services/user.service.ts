import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, IAppConfig } from '@app/core';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {
  DataSourceRequestState,
  toDataSourceRequestString,
  translateDataSourceResultGroups
} from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
export abstract class ACUserService {
  private BASE_URL = '/user';
  public loading: boolean=false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
    ) { }

  public fetch(state: DataSourceRequestState): Observable<GridDataResult> {
    const queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return this.http
      .get(this.endPoint+ `${this.BASE_URL}?${queryStr}`) // Send the state to the server
      .pipe(
        map(
          (Response:any) =>
            <GridDataResult>{
              data: hasGroups
                ? translateDataSourceResultGroups(Response['data'])
                : Response['data'],
              total: Response['total']
            }
        ),
        tap(() => (this.loading = false))
      );
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class GridUserService extends ACUserService {
  constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, 'user',`${appConfig.apiEndpoint}`);
  }
}
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: IAppConfig,
    private http: HttpClient)
     { }
  create(user: any) {
    return this.http.post(`${this.appConfig.apiEndpoint}`+'/user', user).pipe(map(res => res || {}));
  }
  getUser(id: any) {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/user/' + id).pipe(map(res => res || {}));
  }
  update(user: any) {
    return this.http
      .put(`${this.appConfig.apiEndpoint}`+'/user/' + user.user_ID, user)
      .pipe(map(res => res || {}));
  }
  delete(id: any) {
    return this.http.delete(`${this.appConfig.apiEndpoint}`+'/user/' + id).pipe(map(res => res || {}));
  }
  getUsers() {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/user').pipe(map(res => res || {}));
  }
  getUserOf_Office_UserLoggedIn() {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/user/office/getuserof_office_userloggedin').pipe(map(res => res || {}));
  }

  getUsersOffices(id: any) {
    return this.http.get(`${this.appConfig.apiEndpoint}`+'/user/office/' + id).pipe(map(res => res || {}));
  }

  // searchOffice(myData: string) {
  //   // return this.http.get('/api/book').pipe(map(res => res.json()));
  //   // const data = { term: '2' };
  //   // return this.http.get('/api/book/byname' + myData);
  //   return this.http
  //     .get('/api/office/search', { params: myData })
  //     .pipe(map(res => res.json()));
  //   // return this.http.get('/api/book/searchbook', { params: data });

  //   // .pipe(map(res => res.json()));
  // }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
