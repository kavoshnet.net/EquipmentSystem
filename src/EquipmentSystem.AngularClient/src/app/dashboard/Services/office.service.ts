import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, IAppConfig } from '@app/core';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {
  DataSourceRequestState,
  toDataSourceRequestString,
  translateDataSourceResultGroups
} from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
export abstract class ACOfficeService {
  private BASE_URL = `/office`;

  public loading: boolean = false;

  constructor(
    private http: HttpClient,
    protected tableName: string,
    private endPoint: string
  ) { }

  public fetch(state: DataSourceRequestState): Observable<GridDataResult> {
    const queryStr = `${toDataSourceRequestString(state)}`; // Serialize the state
    const hasGroups = state.group && state.group.length;
    this.loading = true;
    return (
      this.http
        .get(this.endPoint + `${this.BASE_URL}?${queryStr}`) // Send the state to the server
        .pipe(
          map(
            (Response: any) =>
              <GridDataResult>{
                data: hasGroups
                  ? translateDataSourceResultGroups(Response['data'])
                  : Response['data'],
                total: Response['total']
              }
          ),
          tap(() => (this.loading = false))
        )
    );
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class GridOfficeService extends ACOfficeService {
  constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: IAppConfig) {
    super(http, 'office', `${appConfig.apiEndpoint}`);
  }
}
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************
// ********************************************************************************************

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class OfficeService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: IAppConfig,
    private http: HttpClient
  ) { }

  create(office: any) {
    return this.http.post(`${this.appConfig.apiEndpoint}/office`, office);
  }
  getOffice(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office/` + id)
      .pipe(map(res => res || {}));
  }
  update(office: any) {
    return this.http
      .put(`${this.appConfig.apiEndpoint}/office/` + office.office_ID, office)
      .pipe(map(res => res || {}));
  }
  delete(id: any) {
    return this.http
      .delete(`${this.appConfig.apiEndpoint}/office/` + id)
      .pipe(map(res => res || {}));
  }
  getOffices() {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office`)
      .pipe(map(res => res || {}));
  }

  getOfficesRoot() {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office/root`)
      .pipe(map(res => res || {}));
  }

  getOfficeRootUserLoggedIn() {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office/rootuserloggedin`)
      .pipe(map(res => res || {}));
  }
  getOfficesParents(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office/parents/` + id)
      .pipe(map(res => res || {}));
  }

  getOfficesChild(id: any) {
    return this.http
      .get(`${this.appConfig.apiEndpoint}/office/child/` + id)
      .pipe(map(res => res || {}));
  }
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
