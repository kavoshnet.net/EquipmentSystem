import { TestBed } from '@angular/core/testing';

import { UserEquipmentService } from './user-equipment.service';

describe('UserEquipmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserEquipmentService = TestBed.get(UserEquipmentService);
    expect(service).toBeTruthy();
  });
});
