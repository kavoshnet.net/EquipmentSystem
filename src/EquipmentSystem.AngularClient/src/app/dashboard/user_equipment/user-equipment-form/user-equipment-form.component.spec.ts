import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEquipmentFormComponent } from './user-equipment-form.component';

describe('UserEquipmentFormComponent', () => {
  let component: UserEquipmentFormComponent;
  let fixture: ComponentFixture<UserEquipmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEquipmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEquipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
