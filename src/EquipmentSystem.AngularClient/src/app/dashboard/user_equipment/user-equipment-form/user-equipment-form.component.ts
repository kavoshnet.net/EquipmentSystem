import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserEquipmentService } from '../../Services/user-equipment.service';
import { EquipmentService } from './../../Services/equipment.service';
import { UserService } from './../../Services/user.service';
// ------------------detail option ---------------------------------
const CASE_NAME: Array<string> = [
  'case',
  'laptop',
  'allinone',
  'thinclient',
  'server'
];
const PRINTER_NAME = 'printer';
const SCANER_NAME = 'scaner';
const PHOTOCAMERA_NAME = 'canon';
// ------------------detail option ---------------------------------
@Component({
  selector: 'app-user-equipment-form',
  templateUrl: './user-equipment-form.component.html',
  styleUrls: ['./user-equipment-form.component.css']
})
export class UserEquipmentFormComponent implements OnInit, OnDestroy {
  users: any[] = [];
  // tempusers: any[];
  // ------------------detail option ---------------------------------
  iscase = false;
  isprinter = false;
  isscaner = false;
  iscamera = false;
  // ------------------detail option ---------------------------------
  user_equipment: any = {};
  // equipments: any[];
  equipments0: any[] = [];
  // tempequipments0: any[];
  equipments1: any[] = [];
  // tempequipments1: any[];
  equipments2: any[] = [];
  // tempequipments2: any[];
  // filterVal: any;
  _id1: any[] = [];
  _id2: any[] = [];
  _id3: any[] = [];
  equip_Name: string = '';
  // officeName: string;
  // userName: string;

  // filter option ------------------------------------------------------------

  // filter option user section------------------------------------------------------------
  public userMultiCtrl: FormControl = new FormControl();
  public userMultiFilterCtrl: FormControl = new FormControl();
  public filteredUsersMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  // Compiler error
  @ViewChild('multiSelectUser', { static: false }) multiSelectUser!: MatSelect;
  // filter option office section------------------------------------------------------------

  // filter option equipment0 section------------------------------------------------------------
  public equipment0MultiCtrl: FormControl = new FormControl();
  public equipment0MultiFilterCtrl: FormControl = new FormControl();
  public filteredEquipments0Multi: ReplaySubject<any[]> = new ReplaySubject<
    any[]
  >(1);
  // Compiler error
  @ViewChild('multiSelectEquipment0', { static: false }) multiSelectEquipment0!: MatSelect;
  // filter option equipment0 section------------------------------------------------------------

  // filter option equipment1 section------------------------------------------------------------
  public equipment1MultiCtrl: FormControl = new FormControl();
  public equipment1MultiFilterCtrl: FormControl = new FormControl();
  public filteredEquipments1Multi: ReplaySubject<any[]> = new ReplaySubject<
    any[]
  >(1);
  // Compiler error
  @ViewChild('multiSelectEquipment1', { static: false }) multiSelectEquipment1!: MatSelect;
  // filter option equipment1 section------------------------------------------------------------

  // filter option equipment2 section------------------------------------------------------------
  public equipment2MultiCtrl: FormControl = new FormControl();
  public equipment2MultiFilterCtrl: FormControl = new FormControl();
  public filteredEquipments2Multi: ReplaySubject<any[]> = new ReplaySubject<
    any[]
  >(1);
  // Compiler error
  @ViewChild('multiSelectEquipment2', { static: false }) multiSelectEquipment2!: MatSelect;
  // filter option equipment2 section------------------------------------------------------------

  // filter option public setting------------------------------------------------------------
  placeholderLabel = 'جستجو';
  noEntriesFoundLabel = 'هیچ داده ای پیدا نشد';
  clearSearchInput = false;
  disableInitialFocus = false;
  private _onDestroy = new Subject<void>();
  // filter option public setting------------------------------------------------------------

  // filter option ------------------------------------------------------------

  constructor(
    private router: Router,
    _route: ActivatedRoute,
    private equipmentService: EquipmentService,
    private userequipmentService: UserEquipmentService,
    private userServices: UserService,
  ) {
    _route.params.subscribe(
      p => {
        this.user_equipment.userEquip_ID = +p['id'];
      },
      err => {
        if (err.status === 404) {
          this.router.navigate(['/user_equipment']);
        }
      }
    );
  }
  ngOnInit() {
    if (this.user_equipment.userEquip_ID) {
      this.userequipmentService
        .getUserEquipment(this.user_equipment.userEquip_ID)
        .subscribe(b => {
          this.user_equipment = b;
          this.equip_Name = this.user_equipment.equipment.equip_Name;
          this.checkIsDetail(this.equip_Name);
          // this.equipmentServices
          //   .getEquipment(this.user_equipment.equip_ID)
          //   .subscribe(e => {
          //     this.equip_Name = e.equip_Name;
          //     if (this.equip_Name.toLocaleLowerCase() === CASE_NAME) {
          //       this.iscase = true;
          //     } else {
          //       this.iscase = false;
          //     }
          //   });
        });
    }
    //////////////////////////////////////////////////////////////////////////////
    // this.equipmentServices.getEquipments().subscribe(e => {
    //   this.equipments = e.data;
    // });
    //////////////////////////////////////////////////////////////////////////////
    this.userServices.getUsers().subscribe((u: any) => {
      this.users = u.data;
      // filter option office section------------------------------------------------------------
      this.filteredUsersMulti.next(this.users.slice());
      // filter option office section------------------------------------------------------------
      // this.tempusers = u.data;
    });

    this.equipmentService.getEquipmentsRoot().subscribe((s: any) => {
      this.equipments0 = s.data;
      // filter option equipment0 section------------------------------------------------------------
      this.filteredEquipments0Multi.next(this.equipments0.slice());
      // filter option equipment0 section------------------------------------------------------------
      // this.tempequipments0 = s.data;
    });
    // this.getEquipName();
    this._id1 = [];
    this._id2 = [];
    this._id3 = [];

    // filter option user section------------------------------------------------------------
    this.userMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUsersMulti();
      });
    // filter option user section------------------------------------------------------------

    // filter option equipment0  section------------------------------------------------------------
    this.equipment0MultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEquipments0Multi();
      });
    // filter option equipment0 section------------------------------------------------------------

    // filter option equipment1  section------------------------------------------------------------
    this.equipment1MultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEquipments1Multi();
      });
    // filter option equipment1 section------------------------------------------------------------

    // filter option equipment2  section------------------------------------------------------------
    this.equipment2MultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEquipments2Multi();
      });
    // filter option equipment2 section------------------------------------------------------------
  }

  // filter option public section------------------------------------------------------------
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  // filter option public section------------------------------------------------------------

  // filter option office section------------------------------------------------------------
  private filterUsersMulti() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.userMultiFilterCtrl.value;
    if (!search) {
      this.filteredUsersMulti.next(this.users.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredUsersMulti.next(
      this.users.filter(
        user => user.user_Lname.toLowerCase().indexOf(search) > -1
      )
    );
  }
  // filter option office section------------------------------------------------------------

  // filter option equipment0 section------------------------------------------------------------
  private filterEquipments0Multi() {
    if (!this.equipments0) {
      return;
    }
    // get the search keyword
    let search = this.equipment0MultiFilterCtrl.value;
    if (!search) {
      this.filteredEquipments0Multi.next(this.equipments0.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEquipments0Multi.next(
      this.equipments0.filter(
        equipment0 => equipment0.equip_Name.toLowerCase().indexOf(search) > -1
      )
    );
  }
  // filter option office section------------------------------------------------------------

  // filter option equipment1 section------------------------------------------------------------
  private filterEquipments1Multi() {
    if (!this.equipments1) {
      return;
    }
    // get the search keyword
    let search = this.equipment1MultiFilterCtrl.value;
    if (!search) {
      this.filteredEquipments1Multi.next(this.equipments1.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEquipments1Multi.next(
      this.equipments1.filter(
        equipment1 => equipment1.equip_Name.toLowerCase().indexOf(search) > -1
      )
    );
  }
  // filter option equipment1 section------------------------------------------------------------

  // filter option equipment2 section------------------------------------------------------------
  private filterEquipments2Multi() {
    if (!this.equipments2) {
      return;
    }
    // get the search keyword
    let search = this.equipment2MultiFilterCtrl.value;
    if (!search) {
      this.filteredEquipments2Multi.next(this.equipments2.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEquipments2Multi.next(
      this.equipments2.filter(
        equipment2 => equipment2.equip_Name.toLowerCase().indexOf(search) > -1
      )
    );
  }
  // filter option equipment1 section------------------------------------------------------------

  // onChangeUSR(event) {
  //   this.filterVal = null;
  // }
  onChangeEQ0(event: any) {
    this.equipments1 = [];
    this.equipments2 = [];
    this._id1 = [];
    this._id2 = [];
    this._id3 = [];
    this.equip_Name = event.source.triggerValue;
    this.checkIsDetail(this.equip_Name);
    // this.filterVal = null;
  }
  onChangeEQ1(event: any) {
    this.equipments1 = [];
    this.equipments2 = [];
    this._id2 = [];
    this._id3 = [];
    this.equip_Name = event.source.triggerValue;
    this.checkIsDetail(this.equip_Name);
    // this.filterVal = null;
    this._id1 = event.value;
    if (this._id1) {
      this.equipmentService.getEquipmentsChild(this._id1).subscribe((s: any) => {
        this.equipments1 = s.data;
        // filter option equipment1 section------------------------------------------------------------
        this.filteredEquipments1Multi.next(this.equipments1.slice());
        // filter option equipment1 section------------------------------------------------------------
        // this.tempequipments1 = s.data;
      });
    }
  }
  onChangeEQ2(event: any) {
    this.equipments2 = [];
    this._id3 = [];
    this.equip_Name = event.source.triggerValue;
    this.checkIsDetail(this.equip_Name);
    // this.filterVal = null;
    this._id2 = event.value;
    if (this._id2) {
      this.equipmentService.getEquipmentsChild(this._id2).subscribe((s: any) => {
        this.equipments2 = s.data;
        // filter option equipment2 section------------------------------------------------------------
        this.filteredEquipments2Multi.next(this.equipments2.slice());
        // filter option equipment2 section------------------------------------------------------------
        // this.tempequipments2 = s.data;
      });
    }
  }

  onChangeEQ3(event: any) {
    this.equip_Name = event.source.triggerValue;
    this._id3 = event.value;
    // this.filterVal = null;
  }

  submit() {
    if (this.user_equipment.userEquip_ID) {
      this.userequipmentService.update(this.user_equipment).subscribe(() => {
        this.router.navigate(['/user_equipment']);
      });
    } else {
      this.user_equipment.userEquip_ID = 0;
      this.userequipmentService.create(this.user_equipment).subscribe(() => {
        this.clearData();
        this.router.navigate(['/user_equipment/new']);
      });
    }
  }
  delete() {
    if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.userequipmentService
        .delete(this.user_equipment.userEquip_ID)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }
  clearData() {
    this.user_equipment.equip_ID = null;
    this.user_equipment.shSerial = null;
    this.user_equipment.shAmval = null;
    this.user_equipment.desc1 = null;
    this.user_equipment.desc2 = null;
    this.user_equipment.desc3 = null;
    this.user_equipment.desc4 = null;
    this.user_equipment.desc5 = null;
    this.user_equipment.desc6 = null;
    this.iscase = false;
    this.isprinter = false;
    this.isscaner = false;
    this.equip_Name = '';
  }
  checkIsDetail(eqname: string) {
    // ---------------------------------case option ------------------------------------
    if (CASE_NAME.some(x => x === eqname.toLocaleLowerCase())) {
      this.iscase = true;
    } else {
      this.iscase = false;
    }
    // ---------------------------------printer option ------------------------------------
    if (eqname.toLocaleLowerCase().search(PRINTER_NAME) >= 0) {
      this.isprinter = true;
    } else {
      this.isprinter = false;
    }
    // ---------------------------------scaner option ------------------------------------
    if (eqname.toLocaleLowerCase().search(SCANER_NAME) >= 0) {
      this.isscaner = true;
    } else {
      this.isscaner = false;
    }
    // ---------------------------------photocamera option ------------------------------------
    if (eqname.toLocaleLowerCase().search(PHOTOCAMERA_NAME) >= 0) {
      this.iscamera = true;
    } else {
      this.iscamera = false;
    }
  }
  // filterListUser(val) {
  //   this.users = this.tempusers.filter(
  //     unit => unit.user_Lname.toLowerCase().indexOf(val.toLowerCase()) > -1
  //   );
  // }
  // filterListEquipment0(val) {
  //   this.equipments0 = this.tempequipments0.filter(
  //     unit => unit.equip_Name.toLowerCase().indexOf(val.toLowerCase()) > -1
  //   );
  // }
  // filterListEquipment1(val) {
  //   this.equipments1 = this.tempequipments1.filter(
  //     unit => unit.equip_Name.toLowerCase().indexOf(val.toLowerCase()) > -1
  //   );
  // }
  // filterListEquipment2(val) {
  //   this.tempequipments2 = this.tempequipments2.filter(
  //     unit => unit.equip_Name.toLowerCase().indexOf(val.toLowerCase()) > -1
  //   );
  // }
  // clear() {
  //   this.filterVal = '';
  //   this.filterListUser('');
  //   this.filterListEquipment0('');
  //   if (this.tempequipments1) {
  //     this.filterListEquipment1('');
  //   }
  //   if (this.tempequipments2) {
  //     this.filterListEquipment2('');
  //   }
  // }
}
