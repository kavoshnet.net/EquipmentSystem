import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEquipmentSearchComponent } from './user-equipment-search.component';

describe('UserEquipmentSearchComponent', () => {
  let component: UserEquipmentSearchComponent;
  let fixture: ComponentFixture<UserEquipmentSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEquipmentSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEquipmentSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
