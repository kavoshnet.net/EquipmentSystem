import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  DataStateChangeEvent,
  GridDataResult
} from '@progress/kendo-angular-grid';
import { DataSourceRequestState } from '@progress/kendo-data-query';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators/switchMap';
import {
  GridSearchUserEquipmentService,
  UserEquipmentService
} from '../../Services/user-equipment.service';
import { CaseDetailComponent } from '../user-equipment-list/user-equipment-list.component';
import { EquipmentService } from './../../Services/equipment.service';
import { OfficeService } from './../../Services/office.service';
import { UserService } from './../../Services/user.service';

const CASE_NAME = 'case';
interface Querystring {
  user_ID: any[];
  office_ID0: any[];
  office_ID1: any[];
  equipment_ID0: any[];
  equipment_ID1: any[];
  equipment_ID2: any[];
  clear: () => void;
}
interface Aggregate {
  Count: number;
  // SumAmount: number;
  // SumOfficeWage: number;
  // SumAddedValue: number;
}

@Component({
  selector: 'app-user-equipment-search',
  templateUrl: './user-equipment-search.component.html',
  styleUrls: ['./user-equipment-search.component.css']
})
export class UserEquipmentSearchComponent implements OnInit {
  public caseDetail: any;
  public loading: boolean=false;
  public state: DataSourceRequestState = {
    skip: 0,
    take: 20
  };
  public query: Observable<GridDataResult> | undefined;
  private stateChange = new BehaviorSubject<any>(this.state);

  // pagination configuration
  public buttonCount = 20;
  public info = true;
  public type: 'numeric' | 'input' = 'input';
  public pageSizes = [5, 10, 20, 40, 100];
  public previousNext = true;
  // multi select basic configuration
  dropdownBaseSettings = {};

  offices0: any[] = [];
  // multi select office configuration
  selectedOffices0: any[] = [];
  dropdownOffice0Settings = new Object();
  // multi select office configuration

  offices1: any[] = [];
  // multi select office1 configuration
  selectedOffices1: any[] = [];
  dropdownOffice1Settings = new Object();
  // multi select office1 configuration

  users: any[] = [];
  // multi select user configuration
  selectedUsers: any[] = [];
  dropdownUserSettings = new Object();
  // multi select user configuration

  equipments0: any[] = [];
  // multi select equipments0 configuration
  selectedEquipments0: any[] = [];
  dropdownEquipment0Settings = new Object();
  // multi select equipments0 configuration

  equipments1: any[] = [];
  // multi select equipments1 configuration
  selectedEquipments1: any[] = [];
  dropdownEquipment1Settings = new Object();
  // multi select equipments1 configuration

  equipments2: any[] = [];
  // multi select equipments2 configuration
  selectedEquipments2: any[] = [];
  dropdownEquipment2Settings = new Object();
  // multi select equipments2 configuration

  myval = {} as Aggregate;

  _id1: any[] = [];
  _id2: any[] = [];
  _offid1: any[] = [];

  _queryString: Querystring = {
    user_ID: [],
    office_ID0: [],
    office_ID1: [],
    equipment_ID0: [],
    equipment_ID1: [],
    equipment_ID2: [],
    clear: (): void => {
      this._queryString.user_ID = [];
      this._queryString.office_ID0 = [];
      this._queryString.office_ID1 = [];
      this._queryString.equipment_ID0 = [];
      this._queryString.equipment_ID1 = [];
      this._queryString.equipment_ID2 = [];
    }
  };

  // filter option ------------------------------------------------------------

  constructor(
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService,
    private equipmentService: EquipmentService,
    private userEquipmentService: UserEquipmentService,
    private gridSearchUserEquipmentService: GridSearchUserEquipmentService,
    private dialog: MatDialog
  ) {
    this.userEquipmentService.getUserEquipments().subscribe((u:any) => {
      this.userequipments = u.data;
    });
  }

  myData: any;
  userequipments: any[]=[];
  ngOnInit() {
    // multi select basic configuration

    this.dropdownBaseSettings = {
      singleSelection: false,
      selectAllText: 'انتخاب همه',
      unSelectAllText: 'عدم انتخاب همه',
      searchPlaceholderText: 'جستجو',
      filterSelectAllText: 'انتخاب همه داده های فیلتر شده',
      filterUnSelectAllText: 'عدم انتخاب همه داده های فیلتر شده',
      noDataLabel: 'داده ای یافت نشد',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      searchAutofocus: true,
      // lazyLoading: true,
      classes: 'myclass custom-class'
    };
    // multi select basic configuration
    this.dropdownOffice0Settings = Object.assign({}, this.dropdownBaseSettings);


    // this.dropdownOffice0Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'office_Name',
    //   primaryKey:'office_ID'
    // }
    this.dropdownOffice0Settings['text'] = 'انتخاب اداره';
    this.dropdownOffice0Settings['labelKey'] = 'office_Name';
    this.dropdownOffice0Settings['primaryKey'] = 'office_ID';

    // multi select office0 configuration

    // multi select office configuration
    this.dropdownOffice1Settings = Object.assign({}, this.dropdownBaseSettings);

    // this.dropdownOffice1Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'office_Name',
    //   primaryKey:'office_ID'
    // }

    this.dropdownOffice1Settings['text'] = 'انتخاب اداره';
    this.dropdownOffice1Settings['labelKey'] = 'office_Name';
    this.dropdownOffice1Settings['primaryKey'] = 'office_ID';

    // multi select office configuration

    this.officeService.getOfficesRoot().subscribe((s:any) => {
      this.offices0 = s.data;
    });

    // multi select user configuration
    this.dropdownUserSettings = Object.assign({}, this.dropdownBaseSettings);

    // this.dropdownUserSettings={
    //   text:'انتخاب اداره',
    //   labelKey:'user_Lname',
    //   primaryKey:'user_ID'
    // }

    this.dropdownUserSettings['text'] = 'انتخاب کاربر';
    this.dropdownUserSettings['labelKey'] = 'user_Lname';
    this.dropdownUserSettings['primaryKey'] = 'user_ID';

    // this.dropdownUserSettings['searchBy'] = 'user_Lname';
    // multi select user configuration

    // multi select equipment0 configuration
    this.dropdownEquipment0Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment0Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'equip_Name',
    //   primaryKey:'equip_ID'
    // }

    this.dropdownEquipment0Settings['text'] = 'انتخاب نوع تجهیزات';
    this.dropdownEquipment0Settings['labelKey'] = 'equip_Name';
    this.dropdownEquipment0Settings['primaryKey'] = 'equip_ID';

    // multi select equipment0 configuration

    // multi select equipment1 configuration
    this.dropdownEquipment1Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment1Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'equip_Name',
    //   primaryKey:'equip_ID'
    // }

    this.dropdownEquipment1Settings['text'] = 'انتخاب نوع برند';
    this.dropdownEquipment1Settings['labelKey'] = 'equip_Name';
    this.dropdownEquipment1Settings['primaryKey'] = 'equip_ID';

    // multi select equipment1 configuration

    // multi select equipment2 configuration

    this.dropdownEquipment2Settings = Object.assign(
      {},
      this.dropdownBaseSettings
    );

    // this.dropdownEquipment2Settings={
    //   text:'انتخاب اداره',
    //   labelKey:'equip_Name',
    //   primaryKey:'equip_ID'
    // }

    this.dropdownEquipment2Settings['text'] = 'انتخاب نوع مدل';
    this.dropdownEquipment2Settings['labelKey'] = 'equip_Name';
    this.dropdownEquipment2Settings['primaryKey'] = 'equip_ID';
    // multi select equipment2 configuration

    this.getUsers();

    this.equipmentService.getEquipmentsRoot().subscribe((s:any) => {
      this.equipments0 = s.data;
    });
    this._id1 = [];
    this._id2 = [];
    this._offid1 = [];
  }

  searchUserEquipment() {
    // multi select basic configuration
    this.mapQueryString();
    // multi select basic configuration

    this.myData = 'user_ID=' + this._queryString.user_ID + '&office_ID=';
    if (this._queryString.office_ID1.length !== 0) {
      this.myData += this._queryString.office_ID1;
    } else if (this._queryString.office_ID0.length !== 0) {
      this.myData += this._queryString.office_ID0;
    }
    this.myData += '&equipment_ID=';
    if (this._queryString.equipment_ID2.length !== 0) {
      this.myData += this._queryString.equipment_ID2;
    } else if (this._queryString.equipment_ID1.length !== 0) {
      this.myData += this._queryString.equipment_ID1;
    } else if (this._queryString.equipment_ID0.length !== 0) {
      this.myData += this._queryString.equipment_ID0;
    }

    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state =>
        this.gridSearchUserEquipmentService.fetch(state, this.myData)
      ),
      tap<any>(agg => {
        this.loading = false;
        const queryParams: any = {};
        if (this._queryString.user_ID) {
          queryParams.user_ID = this._queryString.user_ID;
        }
        if (this._queryString.office_ID1.length !== 0) {
          queryParams.office_ID = this._queryString.office_ID1;
        } else if (this._queryString.office_ID0.length !== 0) {
          queryParams.office_ID = this._queryString.office_ID0;
        }
        if (this._queryString.equipment_ID2.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID2;
        } else if (this._queryString.equipment_ID1.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID1;
        } else if (this._queryString.equipment_ID0.length !== 0) {
          queryParams.equipment_ID = this._queryString.equipment_ID0;
        }
        this.router.navigate(['/user_equipment/search'], {
          queryParams: queryParams
        });
        this.myval.Count = agg.count;
      })
    );
  }
  onChangeOF0(_items: any) {
    this.mapQueryString();

    this.offices1 = [];
    // multi select basic configuration
    this.selectedOffices1 = [];
    // multi select basic configuration
    this._queryString.office_ID1 = [];
    this._offid1 = [];
    this.getUsers();
  }
  onChangeOF1(_items: any) {
    this.mapQueryString();
    this.offices1 = [];
    // multi select basic configuration

    // multi select basic configuration
    this.selectedOffices1 = [];
    this._offid1 = this._queryString.office_ID0;
    if (this._offid1.length > 0) {
      this.officeService.getOfficesChild(this._offid1).subscribe((s:any) => {
        this.offices1 = s.data;
      });
    }
    this.getUsers();
    this.searchUserEquipment();
  }

  onChangeEQ0(_items: any) {
    // multi select basic configuration
    this.mapQueryString();
    this.equipments1 = [];
    // multi select basic configuration
    this.selectedEquipments1 = [];
    this.equipments2 = [];
    // multi select basic configuration
    this.selectedEquipments2 = [];
    this._queryString.equipment_ID1 = [];
    this._queryString.equipment_ID2 = [];
    this._id1 = [];
    this._id2 = [];
  }
  onChangeEQ1(_items: any) {
    // multi select basic configuration
    this.mapQueryString();
    this.equipments2 = [];
    // multi select basic configuration
    this.selectedEquipments2 = [];
    this._queryString.equipment_ID2 = [];
    this._id2 = [];

    this.equipments1 = [];
    // multi select basic configuration
    this.selectedEquipments1 = [];
    this._queryString.equipment_ID1 = [];

    this._id1 = this._queryString.equipment_ID0;
    if (this._id1.length > 0) {
      this.equipmentService.getEquipmentsChild(this._id1).subscribe((s:any) => {
        this.equipments1 = s.data;
      });
    }
    this.searchUserEquipment();
  }
  onChangeEQ2(_items: any) {
    // multi select basic configuration
    this.mapQueryString();
    this.equipments2 = [];
    // multi select basic configuration
    this.selectedEquipments2 = [];
    this._queryString.equipment_ID2 = [];

    this._id2 = this._queryString.equipment_ID1;
    if (this._id2.length > 0) {
      this.equipmentService.getEquipmentsChild(this._id2).subscribe((s:any) => {
        this.equipments2 = s.data;
      });
    }
    this.searchUserEquipment();
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.stateChange.next(state);
  }

  getUsers() {
    this.mapQueryString();
    if (
      !this._queryString.office_ID0 ||
      this._queryString.office_ID0.length === 0
    ) {
      // multi select basic configuration
      this.selectedUsers = [];
      this._queryString.user_ID = [];
      this.userService.getUsers().subscribe((u:any) => {
        this.users = u.data;
      });
    } else if (
      !this._queryString.office_ID1 ||
      this._queryString.office_ID1.length === 0
    ) {
      this.userService
        .getUsersOffices(this._queryString.office_ID0)
        .subscribe((u:any) => {
          this.users = u.data;
        });
    } else {
      this.userService
        .getUsersOffices(this._queryString.office_ID1)
        .subscribe((u:any) => {
          this.users = u.data;
        });
    }
  }
  // multi select basic configuration
  mapQueryString() {
    this._queryString.office_ID0 = this.selectedOffices0
      ? this.selectedOffices0.map(t => t.office_ID)
      : [];

    this._queryString.office_ID1 = this.selectedOffices1
      ? this.selectedOffices1.map(t => t.office_ID)
      : [];

    this._queryString.user_ID = this.selectedUsers
      ? this.selectedUsers.map(t => t.user_ID)
      : [];

    this._queryString.equipment_ID0 = this.selectedEquipments0
      ? this.selectedEquipments0.map(t => t.equip_ID)
      : [];

    this._queryString.equipment_ID1 = this.selectedEquipments1
      ? this.selectedEquipments1.map(t => t.equip_ID)
      : [];

    this._queryString.equipment_ID2 = this.selectedEquipments2
      ? this.selectedEquipments2.map(t => t.equip_ID)
      : [];
  }
  // multi select basic configuration
  isCase(text: any): boolean {
    if (text.toLocaleLowerCase() === CASE_NAME) {
      return true;
    } else {
      return false;
    }
  }

  getCaseDetail(_id: any) {
    // this.userEquipmentService.getUserEquipment(_id).subscribe(u => {
    //   this.caseDetail = u;
    // });
    this.caseDetail = this.userequipments.find(
      item => item.userEquip_ID === _id
    );
    // if (this.caseDetail === null) {
    //   this.caseDetail.userName = '';
    //   this.caseDetail.desc1 = '';
    //   this.caseDetail.desc2 = '';
    //   this.caseDetail.desc3 = '';
    //   this.caseDetail.desc4 = '';
    // }
  }
  openDialog(_id: any): void {
    this.getCaseDetail(_id);
    const dialogRef = this.dialog.open(CaseDetailComponent, {
      width: '500px',
      data: {
        UserName:
          this.caseDetail.user !== null
            ? this.caseDetail.user.user_Fname +
              ' ' +
              this.caseDetail.user.user_Lname
            : 'مقدار تعیین نشده است',
        NORAM:
          this.caseDetail.desc1 !== null
            ? this.caseDetail.desc1
            : 'مقدار تعیین نشده است',
        RAM:
          this.caseDetail.desc2 !== null
            ? this.caseDetail.desc2
            : 'مقدار تعیین نشده است',
        CPU:
          this.caseDetail.desc3 !== null
            ? this.caseDetail.desc3
            : 'مقدار تعیین نشده است',
        VGA:
          this.caseDetail.desc4 !== null
            ? this.caseDetail.desc4
            : 'مقدار تعیین نشده است',
        OS:
          this.caseDetail.desc5 !== null
            ? this.caseDetail.desc5
            : 'مقدار تعیین نشده است'
      }
    });
    dialogRef.afterClosed().subscribe(_result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }
}
