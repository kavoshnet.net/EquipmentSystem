import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CallProtectedApiComponent } from './call-protected-api/call-protected-api.component';
import { GrdfilterPipe } from './CustomPipe/grdfilter.pipe';
import { MomentJalaaliPipe } from './CustomPipe/moment-jalaali.pipe';
import { MulticheckFilterComponent } from './CustomPipe/multicheck-filter/multicheck-filter.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardEquipmentsComponent } from './dashboard/dashboard-equipments/dashboard-equipments.component';
import { EquipmentFormComponent } from './equipment/equipment-form/equipment-form.component';
import { EquipmentListComponent } from './equipment/equipment-list/equipment-list.component';
import { OfficeFormComponent } from './office/office-form/office-form.component';
import { OfficeListComponent } from './office/office-list/office-list.component';
import { ProtectedPageComponent } from './protected-page/protected-page.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserEquipmentFormComponent } from './user_equipment/user-equipment-form/user-equipment-form.component';
import { CaseDetailComponent, UserEquipmentListComponent } from './user_equipment/user-equipment-list/user-equipment-list.component';
import { UserEquipmentSearchComponent } from './user_equipment/user-equipment-search/user-equipment-search.component';
import { UserEquReqFormComponent } from './user_equ_req/user-equ-req-form/user-equ-req-form.component';
import { UserEquReqListComponent } from './user_equ_req/user-equ-req-list/user-equ-req-list.component';
import { UserEquReqDetailsComponent } from './user_equ_req/user-equ-req-details/user-equ-req-details.component';

@NgModule({
  imports: [CommonModule, SharedModule, DashboardRoutingModule],
  exports: [
    // common and shared components/directives/pipes between more than one module and components will be listed here.
  ],
  declarations: [
    ProtectedPageComponent,
    CallProtectedApiComponent,
    OfficeFormComponent,
    OfficeListComponent,
    UserFormComponent,
    UserListComponent,
    EquipmentFormComponent,
    EquipmentListComponent,
    UserEquipmentFormComponent,
    UserEquipmentListComponent,
    UserEquipmentSearchComponent,
    DashboardEquipmentsComponent,
    CaseDetailComponent,
    MulticheckFilterComponent,
    GrdfilterPipe,
    MomentJalaaliPipe,
    UserEquReqFormComponent,
    UserEquReqListComponent,
    UserEquReqDetailsComponent
  ]
})
export class DashboardModule {}
