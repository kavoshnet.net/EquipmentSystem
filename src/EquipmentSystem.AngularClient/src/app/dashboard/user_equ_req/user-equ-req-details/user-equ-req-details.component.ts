import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserEquipmentreqService } from '../../Services/user-equipmentreq.service';
import { AuthService } from '@app/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-equ-req-details',
  templateUrl: './user-equ-req-details.component.html',
  styleUrls: ['./user-equ-req-details.component.css']
})
export class UserEquReqDetailsComponent implements OnInit {
  user_equipmentReq: any = {};
  equip_Name: string = '';

  isAdmin = false;
  isUser = false;
  isEditor = false;
  isLoggedIn = false;
  subscription: Subscription | null = null;
  displayName = '';

  constructor(
    private router: Router,
    _route: ActivatedRoute,
    private authService: AuthService,
    private userequipmentreqService: UserEquipmentreqService
  ) {
    _route.params.subscribe(
      p => {
        this.user_equipmentReq.userEquipReq_ID = +p['id'];
      },
      err => {
        if (err.status === 404) {
          this.router.navigate(['/user_equipmentreq']);
        }
      }
    );
  }
  ngOnInit() {

    this.subscription = this.authService.authStatus$.subscribe(status => {
      this.isLoggedIn = status;
      if (status) {
        const authUser = this.authService.getAuthUser();
        this.displayName = authUser ? authUser.displayName : '';
        this.isAdmin = this.authService.isAuthUserInRole('Admin');
        this.isUser = this.authService.isAuthUserInRole('User');
        this.isEditor = this.authService.isAuthUserInRole('Editor');
        // this.userId = authUser ? authUser.userId : undefined;
        // if (this.userId !== undefined) {
        //   debugger;
        //   this.userService.getUser(this.userId).subscribe((b:any) => {
        //     this.user = b;
        //   });
        //   this.officeName = this.user.office.Office_Name;
        // }
      }
    });

    if (this.user_equipmentReq.userEquipReq_ID) {
      this.userequipmentreqService
        .getUserEquipmentReqReq(this.user_equipmentReq.userEquipReq_ID)
        .subscribe(b => {
          this.user_equipmentReq = b;
          //this.equip_Name = this.user_equipmentReq.equipmentReq.equip_Name;
        });
    }
  }
  insertReq(){
    if (this.user_equipmentReq.userRecAction=="افزودن") {
      this.userequipmentreqService
        .insertReq(this.user_equipmentReq)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }

  updateReq(){
    if (this.user_equipmentReq.userRecAction=="ویرایش") {
      this.userequipmentreqService
        .updateReq(this.user_equipmentReq)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }
  deleteReq() {
    if (this.user_equipmentReq.userRecAction=="حذف") {
      if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.userequipmentreqService
        .deleteReq(this.user_equipmentReq.userEquipReq_ID)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }
  }

  rejectReq() {
    this.userequipmentreqService
        .rejectReq(this.user_equipmentReq)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
  }
  submit() {
    if (this.user_equipmentReq.userEquip_ID) {
      this.userequipmentreqService
        .update(this.user_equipmentReq)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    } else {
      this.user_equipmentReq.userEquipReq_ID = 0;
      this.userequipmentreqService
        .create(this.user_equipmentReq)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }
  delete() {
    if (confirm('آیا برای حذف اطمینان دارید؟')) {
      this.userequipmentreqService
        .delete(this.user_equipmentReq.userEquip_ID)
        .subscribe(() => {
          this.router.navigate(['/user_equipment']);
        });
    }
  }
}
