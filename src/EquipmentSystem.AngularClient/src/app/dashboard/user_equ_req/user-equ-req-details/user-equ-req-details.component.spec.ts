import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEquReqDetailsComponent } from './user-equ-req-details.component';

describe('UserEquReqDetailsComponent', () => {
  let component: UserEquReqDetailsComponent;
  let fixture: ComponentFixture<UserEquReqDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEquReqDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEquReqDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
