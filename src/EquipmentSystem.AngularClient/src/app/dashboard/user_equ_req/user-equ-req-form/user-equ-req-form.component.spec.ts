import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEquReqFormComponent } from './user-equ-req-form.component';

describe('UserEquReqFormComponent', () => {
  let component: UserEquReqFormComponent;
  let fixture: ComponentFixture<UserEquReqFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEquReqFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEquReqFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
