import { Component /*, Inject*/, OnInit } from '@angular/core';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '@app/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { CompositeFilterDescriptor, DataSourceRequestState, distinct, filterBy } from '@progress/kendo-data-query';
import { Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators/switchMap';
import { GridUserEquipmentReqService, UserEquipmentreqService } from '../../Services/user-equipmentreq.service';
const CASE_NAME: Array<string> = [
  'case',
  'laptop',
  'allinone',
  'thinclient',
  'server'
];

@Component({
  selector: 'app-user-equ-req-list',
  templateUrl: './user-equ-req-list.component.html',
  styleUrls: ['./user-equ-req-list.component.css']
})
export class UserEquReqListComponent implements OnInit {

  public caseDetail: any;
  public loading: boolean = false;

  public state: DataSourceRequestState = {
    skip: 0,
    take: 40
    // sort: [{ field: 'equip_ID', dir: 'asc' }]
  };

  public query: Observable<GridDataResult>;

  private stateChange = new BehaviorSubject<any>(this.state);

  // pagination configuration
  public buttonCount = 40;
  public info = true;
  public type: 'numeric' | 'input' = 'input';
  public pageSizes = [5, 10, 20, 40, 100];
  public previousNext = true;

  // -----------------grid filter menu  --------------------------------
  public filter: CompositeFilterDescriptor | undefined;
  // public gridData: any[] = filterBy(sampleProducts, this.filter);
  public myData: any[] = [];
  public gridData: any[] = [];
  // public categories: any[] = distinct(sampleProducts, 'CategoryID').map(
  //   item => item.Category
  // );

  isAdmin = false;
  isUser = false;
  isEditor = false;
  isLoggedIn = false;
  subscription: Subscription | null = null;
  displayName = '';

  public users: any[] = [];
  public offices: any[] = [];
  public equipments: any[] = [];

  public _querystring: string = '';

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    // this.gridData = filterBy(this.myData, filter);

    this.gridData = filterBy(this.myData, this.filter);

    this.offices = distinct(this.gridData, 'userReq.office_ID').map(
      item => item.user.office
    );

    this.users = distinct(this.gridData, 'userReq_ID').map(item => item.user);

    this.equipments = distinct(this.gridData, 'equipReq_ID').map(
      item => item.equipment
    );
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.myData, fieldName).map(item => item[fieldName]);
  }
  // -----------------grid filter menu  --------------------------------
  constructor(
    private gridUserEquipmentReqService: GridUserEquipmentReqService,
    private userEquipmentreqService: UserEquipmentreqService,
    private authService: AuthService,
    // private dialog: MatDialog
  ) {
    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state =>
        this.gridUserEquipmentReqService.fetch(state, this._querystring)
      ),
      tap(() => {
        this.loading = false;
      })
    );

    // this.gridData = orderBy(this.gridData, this.sort);
    this.userEquipmentreqService.getUserEquipmentReqs().subscribe((u: any) => {
      this.myData = u.data;

      this.offices = distinct(this.myData, 'userReq.office_ID').map(
        item => item.userReq.office
      );

      this.users = distinct(this.myData, 'userReq_ID').map(item => item.userReq);

      this.equipments = distinct(this.myData, 'equipReq_ID').map(
        item => item.equipmentReq
      );
    });
  }
  public runfrom() {
    this.query = this.stateChange.pipe(
      tap(state => {
        this.state = state;
        this.loading = true;
      }),
      switchMap(state =>
        this.gridUserEquipmentReqService.fetch(state, this._querystring)
      ),
      tap(() => {
        this.loading = false;
      })
    );

    // this.gridData = orderBy(this.gridData, this.sort);
    this.userEquipmentreqService.getUserEquipmentReqs().subscribe((u: any) => {
      this.myData = u.data;
    })
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.stateChange.next(state);
  }


  ngOnInit() {
    this.subscription = this.authService.authStatus$.subscribe(status => {
      this.isLoggedIn = status;
      if (status) {
        const authUser = this.authService.getAuthUser();
        this.displayName = authUser ? authUser.displayName : '';
        this.isAdmin = this.authService.isAuthUserInRole('Admin');
        this.isUser = this.authService.isAuthUserInRole('User');
        this.isEditor = this.authService.isAuthUserInRole('Editor');
        // this.userId = authUser ? authUser.userId : undefined;
        // if (this.userId !== undefined) {
        //   debugger;
        //   this.userService.getUser(this.userId).subscribe((b:any) => {
        //     this.user = b;
        //   });
        //   this.officeName = this.user.office.Office_Name;
        // }
      }
    });
  }

  isCase(text: any): boolean {
    if (CASE_NAME.some(x => x === text.toLocaleLowerCase())) {
      return true;
    } else {
      return false;
    }
  }

  getCaseDetail(_id: any) {
    // this.userEquipmentService.getUserEquipment(_id).subscribe(u => {
    //   this.caseDetail = u;
    // });
    this.caseDetail = this.myData.find(item => item.userEquip_ID === _id);
    // if (this.caseDetail === null) {
    //   this.caseDetail.userName = '';
    //   this.caseDetail.desc1 = '';
    //   this.caseDetail.desc2 = '';
    //   this.caseDetail.desc3 = '';
    //   this.caseDetail.desc4 = '';
    // }
  }
  //   openDialog(_id: any): void {
  //     this.getCaseDetail(_id);
  //     const dialogRef = this.dialog.open(CaseDetailComponent, {
  //       width: '500px',
  //       data: {
  //         UserName:
  //           this.caseDetail.user !== null
  //             ? this.caseDetail.user.user_Fname +
  //               ' ' +
  //               this.caseDetail.user.user_Lname
  //             : 'مقدار تعیین نشده است',
  //         NORAM:
  //           this.caseDetail.desc1 !== null
  //             ? this.caseDetail.desc1
  //             : 'مقدار تعیین نشده است',
  //         RAM:
  //           this.caseDetail.desc2 !== null
  //             ? this.caseDetail.desc2
  //             : 'مقدار تعیین نشده است',
  //         CPU:
  //           this.caseDetail.desc3 !== null
  //             ? this.caseDetail.desc3
  //             : 'مقدار تعیین نشده است',
  //         VGA:
  //           this.caseDetail.desc4 !== null
  //             ? this.caseDetail.desc4
  //             : 'مقدار تعیین نشده است',
  //         OS:
  //           this.caseDetail.desc5 !== null
  //             ? this.caseDetail.desc5
  //             : 'مقدار تعیین نشده است'
  //       }
  //     });
  //     dialogRef.afterClosed().subscribe(() => {
  //       // console.log('The dialog was closed');
  //       // this.animal = result;
  //     });
  //   }
  // }

  // export interface CaseDetail {
  //   UserName: string;
  //   NORAM: string;
  //   RAM: string;
  //   CPU: string;
  //   VGA: string;
  //   OS: string;
  // }

  // @Component({
  //   selector: 'app-case-detail.component',
  //   templateUrl: './case-detail.component.html',
  //   styleUrls: ['./case-detail.component.css']
  // })
  // export class CaseDetailComponent {
  //   constructor(
  //     public dialogRef: MatDialogRef<CaseDetailComponent>,
  //     @Inject(MAT_DIALOG_DATA) public data: CaseDetail
  //   ) {}

  //   onNoClick(): void {
  //     this.dialogRef.close();
  //   }
}
