import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEquReqListComponent } from './user-equ-req-list.component';

describe('UserEquReqListComponent', () => {
  let component: UserEquReqListComponent;
  let fixture: ComponentFixture<UserEquReqListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEquReqListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEquReqListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
