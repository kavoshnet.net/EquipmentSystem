import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Compiler error
// import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
//---------------------------------------------------------------------------
import { ChartsModule } from '@progress/kendo-angular-charts';
import { GridModule } from '@progress/kendo-angular-grid';
import { RTL } from '@progress/kendo-angular-l10n';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import 'hammerjs';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

//---------------------------------------------------------------------------

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    GridModule,
    TreeViewModule,
    AngularMultiSelectModule,
    NgxMatSelectSearchModule,
    CdkTableModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    // Compiler error
    // HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // GridModule,
    // TreeViewModule,
    // ChartsModule,
    // NgxMatSelectSearchModule,
    // AngularMultiSelectModule,
  ],
  declarations: [
  ],
  providers: [
    { provide: RTL, useValue: true }],
  exports: [
    CommonModule,
    ChartsModule,
    GridModule,
    TreeViewModule,
    AngularMultiSelectModule,
    NgxMatSelectSearchModule,
    CdkTableModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    // Compiler error
    // HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OthereModule { }
