using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EquipmentSystem.DomainClasses {
    [Table ("UserRoles")]
    public class UserRole {
        public int User_ID { get; set; }

        [ForeignKey ("User_ID")]
        public virtual User User { get; set; }

        public int Role_ID { get; set; }

        [ForeignKey ("Role_ID")]
        public virtual Role Role { get; set; }
    }
}