using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EquipmentSystem.DomainClasses {
    public class UserToken {
        [Key]
        public int UserToken_ID { get; set; }

        public string AccessTokenHash { get; set; }

        public DateTimeOffset AccessTokenExpiresDateTime { get; set; }

        public string RefreshTokenIdHash { get; set; }

        public string RefreshTokenIdHashSource { get; set; }

        public DateTimeOffset RefreshTokenExpiresDateTime { get; set; }

        public int User_ID { get; set; }

        [ForeignKey ("User_ID")]
        public virtual User User { get; set; }
    }
}