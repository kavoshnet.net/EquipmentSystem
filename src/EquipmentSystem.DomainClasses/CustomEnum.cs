using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
namespace EquipmentSystem.DomainClasses {
    public enum ActionEnum {
        [Display (Name = "ثبت")]
        [EnumMember (Value = "ثبت شد")]
        Register = 1,

        [Display (Name = "برگشت")]
        [EnumMember (Value = "پذیرفته نشد")]
        Reject = -1,

        [Display (Name = "افزودن")]
        [EnumMember (Value = "افزودن")]
        Insert = 2,

        [Display (Name = "ویرایش")]
        [EnumMember (Value = "ویرایش")]
        Update = 3,

        [Display (Name = "حذف")]
        [EnumMember (Value = "حذف")]
        Delete = 4,

        [Display (Name = "سرویس")]
        [EnumMember (Value = "درخواست سرویس")]
        Service = 5,
    }
    public static class EnumExtensions {
        public static string GetDisplayName (this Enum enumValue) {
            try {
            return enumValue.GetType ()
            .GetMember (enumValue.ToString ())
            .First ()
            .GetCustomAttribute<DisplayAttribute> ()
            .GetName ();
            } catch (Exception) {

            return "نامعلوم";
            }
        }
    }

    public class CustomEnum {

    }
}