﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EquipmentSystem.DomainClasses {
    [Table ("Users")]
    public class User {
        public User () {
            UserRoles = new HashSet<UserRole> ();
            UserTokens = new HashSet<UserToken> ();
            User_Equipments = new Collection<User_Equipment> ();
        }

        [Key]
        public int User_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string User_Fname { get; set; }

        [Required]
        [StringLength (255)]
        public string User_Lname { get; set; }

        [Required]
        [StringLength (255)]
        public string Username { get; set; }

        [Required]
        [StringLength (255)]
        public string Password { get; set; }

        [Required]
        public bool IsActive { get; set; }

        // [Required]
        public DateTimeOffset? LastLoggedIn { get; set; }

        /// <summary>
        /// every time the user changes his Password,
        /// or an admin changes his Roles or stat/IsActive,
        /// create a new `SerialNumber` GUID and store it in the DB.
        /// </summary>
        //[Required]
        public string SerialNumber { get; set; }

        public int Office_ID { get; set; }

        [ForeignKey ("Office_ID")]
        public Office Office { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<UserToken> UserTokens { get; set; }
        public virtual ICollection<User_Equipment> User_Equipments { get; set; }
        public virtual ICollection<User_EquipmentReq> Req_User_EquipmentReqs { get; set; }
        public virtual ICollection<User_EquipmentReq> Do_User_EquipmentReqs { get; set; }
    }
}