using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EquipmentSystem.DomainClasses;

namespace EquipmentSystem.DomainClasses {
    [Table ("Offices")]
    public class Office {
        [Key]
        public int Office_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string Office_Name { get; set; }

        public int? Parent_Office_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string Office_Code { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public Office () {
            Users = new Collection<User> ();
        }
    }
}