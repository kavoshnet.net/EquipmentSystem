using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EquipmentSystem.DomainClasses;

namespace EquipmentSystem.DomainClasses {
    [Table ("User_EquipmentReqs")]
    public class User_EquipmentReq {
        [Key]
        public int UserEquipReq_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string SHSerialReq { get; set; }

        [Required]
        [StringLength (255)]
        public string SHAmvalReq { get; set; }

        [StringLength (255)]
        public string Desc1Req { get; set; }

        [StringLength (255)]
        public string Desc2Req { get; set; }

        [StringLength (255)]
        public string Desc3Req { get; set; }

        [StringLength (255)]
        public string Desc4Req { get; set; }

        [StringLength (255)]
        public string Desc5Req { get; set; }

        [StringLength (255)]
        public string Desc6Req { get; set; }

        public int? UserEquip_ID { get; set; }

        [ForeignKey ("UserEquip_ID")]
        public virtual User_Equipment Old_User_Equipment { get; set; }

        public int? UserReq_ID { get; set; }

        [ForeignKey ("UserReq_ID")]
        public virtual User UserReq { get; set; }

        public int? UserDo_ID { get; set; }

        [ForeignKey ("UserDo_ID")]
        public virtual User UserDo { get; set; }

        public int? EquipReq_ID { get; set; }

        [ForeignKey ("EquipReq_ID")]
        public virtual Equipment EquipmentReq { get; set; }

        public DateTimeOffset? DateReqAction { get; set; }
        public ActionEnum? UserRecAction { get; set; }
        public DateTimeOffset? DateDoAction { get; set; }
        public ActionEnum? UserDoAction { get; set; }

    }
}