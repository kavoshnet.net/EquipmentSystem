using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EquipmentSystem.DomainClasses {
    [Table ("Roles")]
    public class Role {
        public Role () {
            UserRoles = new HashSet<UserRole> ();
        }

        [Key]
        public int Role_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string Role_Name { get; set; }

        [Required]
        [StringLength (255)]
        public string Role_Caption { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}