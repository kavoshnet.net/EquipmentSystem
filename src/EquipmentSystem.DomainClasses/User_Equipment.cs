using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EquipmentSystem.DomainClasses;

namespace EquipmentSystem.DomainClasses {
    [Table ("User_Equipments")]
    public class User_Equipment {
        [Key]
        public int UserEquip_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string SHSerial { get; set; }

        [Required]
        [StringLength (255)]
        public string SHAmval { get; set; }

        [StringLength (255)]
        public string Desc1 { get; set; }

        [StringLength (255)]
        public string Desc2 { get; set; }

        [StringLength (255)]
        public string Desc3 { get; set; }

        [StringLength (255)]
        public string Desc4 { get; set; }

        [StringLength (255)]
        public string Desc5 { get; set; }

        [StringLength (255)]
        public string Desc6 { get; set; }

        public int User_ID { get; set; }

        [ForeignKey ("User_ID")]
        public User User { get; set; }
        public int Equip_ID { get; set; }

        [ForeignKey ("Equip_ID")]
        public Equipment Equipment { get; set; }

        // public int Office_ID { get; set; }
        // [ForeignKey("Office_ID")]
        // public Office Office { get; set; }
    }
}