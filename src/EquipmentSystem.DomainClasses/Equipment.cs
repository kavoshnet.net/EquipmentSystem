using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EquipmentSystem.DomainClasses {
    [Table ("Equipments")]
    public class Equipment {
        [Key]
        public int Equip_ID { get; set; }

        [Required]
        [StringLength (255)]
        public string Equip_Name { get; set; }

        public int? Parent_Equip_ID { get; set; }

        [ForeignKey ("Parent_Equip_ID")]
        public virtual Equipment Parent { get; set; }
        public ICollection<Equipment> Children { get; set; }
        public ICollection<User_Equipment> User_Equipments { get; set; }
        public Equipment () {
            User_Equipments = new Collection<User_Equipment> ();
            Children = new Collection<Equipment> ();
        }

    }
}