using System;
namespace EquipmentSystem.DomainClasses {
    public class AccountModel {
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Loggedon { get; set; }
        public string Rolename { get; set; }
    }
}