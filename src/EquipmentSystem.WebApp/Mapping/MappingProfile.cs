using AutoMapper;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.WebApp.Controllers.Resources;

namespace EquipmentSystem.WebApp.Mapping {
    public class MappingProfile : Profile {
        private const char OpenPrantes = '(';
        private const char ClosePrantes = ')';
        private const char space = ' ';

        public MappingProfile () {

            // Model to Resourse Mapping
            // User Resources

            // CreateMap<User, UserResource> ().
            // ForMember (u => u.Office, opt => opt.MapFrom (f => new OfficeResource { Office_ID = f.Office.Office_ID, Office_Code = f.Office.Office_Code, Office_Name = f.Office.Office_Name }));

            CreateMap<User, UserResource> ();

            CreateMap<Role, RoleResource> ();

            CreateMap<UserRole, UserRoleResource> ();
            // Office Resources
            CreateMap<Office, OfficeResource> ();

            // Equipment Resources
            CreateMap<Equipment, EquipmentResource> ();
            // ForMember (ue => ue.Children, opt => opt.MapFrom (e => new Equipment { Equip_ID = 0, Equip_Name = "", Parent_Equip_ID = 0 }));

            // User_Equipment Resources
            // CreateMap<User_Equipment, User_EquipmentResource> ().
            // ForMember (ue => ue.Equipment, opt => opt.MapFrom (e => new EquipmentResource { Equip_ID = e.Equipment.Equip_ID, Equip_Name = e.Equipment.Equip_Name, Parent_Equip_ID = e.Equipment.Parent_Equip_ID })).
            // ForMember (ue => ue.User, opt => opt.MapFrom (u => new UserResource {
            //     User_ID = u.User.User_ID, User_Fname = u.User.User_Fname, User_Lname = u.User.User_Lname, Office_ID = u.User.Office.Office_ID,
            //         Office = new OfficeResource { Office_ID = u.User.Office.Office_ID, Office_Name = u.User.Office.Office_Name, Office_Code = u.User.Office.Office_Code }

            // }));

            CreateMap<User_Equipment, User_EquipmentResource> ();
            CreateMap<User_EquipmentReq, User_EquipmentReqResource> ();

            ///////////////////////////////////////////////////////////////////////////
            CreateMap<User_Equipment, User_EquipmentReqResource> ()
                .ForMember (x => x.SHSerialReq, opt => opt.MapFrom (t => t.SHSerial))
                .ForMember (x => x.SHAmvalReq, opt => opt.MapFrom (t => t.SHAmval))
                .ForMember (x => x.Desc1Req, opt => opt.MapFrom (t => t.Desc1))
                .ForMember (x => x.Desc2Req, opt => opt.MapFrom (t => t.Desc2))
                .ForMember (x => x.Desc3Req, opt => opt.MapFrom (t => t.Desc3))
                .ForMember (x => x.Desc4Req, opt => opt.MapFrom (t => t.Desc4))
                .ForMember (x => x.Desc5Req, opt => opt.MapFrom (t => t.Desc5))
                .ForMember (x => x.Desc6Req, opt => opt.MapFrom (t => t.Desc6))
                .ForMember (x => x.UserReq_ID, opt => opt.MapFrom (t => t.User_ID))
                .ForMember (x => x.EquipReq_ID, opt => opt.MapFrom (t => t.Equip_ID))
                .ForMember (x => x.UserEquip_ID, opt => opt.MapFrom (t => t.UserEquip_ID))
                .ForMember (x => x.EquipmentReq, opt => opt.MapFrom (t => t.Equipment))
                .ForMember (x => x.UserReq, opt => opt.MapFrom (t => t.User));

            // Resource to Model Mapping
            CreateMap<UserResource, User> ()
                .ForMember (v => v.User_ID, opt => opt.Ignore ()).ForMember (v => v.Office, opt => opt.Ignore ());
            CreateMap<RoleResource, Role> ()
                .ForMember (v => v.Role_ID, opt => opt.Ignore ());

            CreateMap<OfficeResource, Office> ()
                .ForMember (v => v.Office_ID, opt => opt.Ignore ());
            CreateMap<EquipmentResource, Equipment> ()
                .ForMember (v => v.Equip_ID, opt => opt.Ignore ());
            CreateMap<User_EquipmentResource, User_Equipment> ()
                .ForMember (v => v.UserEquip_ID, opt => opt.Ignore ())
                .ForMember (v => v.Equipment, opt => opt.Ignore ())
                .ForMember (v => v.User, opt => opt.Ignore ());
            CreateMap<User_EquipmentReqResource, User_EquipmentReq> ()
                .ForMember (v => v.UserEquipReq_ID, opt => opt.Ignore ())
                .ForMember (v => v.EquipmentReq, opt => opt.Ignore ())
                .ForMember (v => v.UserReq, opt => opt.Ignore ())
                .ForMember (v => v.UserDo, opt => opt.Ignore ())
                .ForMember (v => v.Old_User_Equipment, opt => opt.Ignore ());

            //////////////////////////////////////////////////////////
            CreateMap<User_EquipmentResource, User_EquipmentReq> ()
                .ForMember (v => v.UserEquipReq_ID, opt => opt.Ignore ())
                .ForMember (v => v.EquipmentReq, opt => opt.Ignore ())
                .ForMember (v => v.UserReq, opt => opt.Ignore ())
                .ForMember (v => v.UserDo, opt => opt.Ignore ())
                ;

            CreateMap<User_EquipmentReqResource, User_Equipment> ()
                .ForMember (x => x.SHSerial, opt => opt.MapFrom (t => t.SHSerialReq))
                .ForMember (x => x.SHAmval, opt => opt.MapFrom (t => t.SHAmvalReq))
                .ForMember (x => x.Desc1, opt => opt.MapFrom (t => t.Desc1Req))
                .ForMember (x => x.Desc2, opt => opt.MapFrom (t => t.Desc2Req))
                .ForMember (x => x.Desc3, opt => opt.MapFrom (t => t.Desc3Req))
                .ForMember (x => x.Desc4, opt => opt.MapFrom (t => t.Desc4Req))
                .ForMember (x => x.Desc5, opt => opt.MapFrom (t => t.Desc5Req))
                .ForMember (x => x.Desc6, opt => opt.MapFrom (t => t.Desc6Req))
                .ForMember (x => x.User_ID, opt => opt.MapFrom (t => t.UserReq_ID))
                .ForMember (x => x.Equip_ID, opt => opt.MapFrom (t => t.EquipReq_ID))
                .ForMember (v => v.UserEquip_ID, opt => opt.Ignore ())
                .ForMember (v => v.Equipment, opt => opt.Ignore ())
                .ForMember (v => v.User, opt => opt.Ignore ());

        }
    }
}