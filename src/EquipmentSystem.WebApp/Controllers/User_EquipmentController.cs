using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.Services;
using EquipmentSystem.WebApp.Controllers.Resources;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace EquipmentSystem.WebApp.Controllers {

    [Route ("api/user_equipment")]
    [EnableCors ("CorsPolicy")]
    [Authorize]
    public class User_EquipmentController : Controller {
        class DashbortDTO {
            public int Equip_ID { get; set; }
            public string Equip_Name { get; set; }
            public int Count { get; set; }
            public string Color { get; set; }
            public DashbortDTO (int _equip_id, string _equip_name, int _count) {
                var random = new Random (); // Don't put this here!
                Equip_ID = _equip_id;
                Equip_Name = _equip_name;
                Count = _count;
                Color = String.Format ("#{0:X6}", random.Next (0x1000000));
            }

        }
        private readonly IMapper mapper;
        private readonly ApplicationDbContext context;
        private readonly IUsersService _usersService;
        private readonly IRolesService _rolesService;

        public User_EquipmentController (
            IMapper mapper,
            ApplicationDbContext context,
            IUsersService usersService,
            IRolesService rolesService
        ) {

            this.mapper = mapper;
            this.context = context;
            this._usersService = usersService;
            this._rolesService = rolesService;

        }

        [HttpGet]
        public async Task<DataSourceResult> GetUser_Equipment ([DataSourceRequest] DataSourceRequest request) {

            var _uid = _usersService.GetCurrentUserId ();
            // var roleName = _rolesService.IsUserInRoleAsync (_uid, "Admin");
            // var ID = User.FindFirst (ClaimTypes.UserData)?.Value;
            // var _uid = long.Parse (ID);
            DataSourceResult user_equipments = null;
            user_equipments = await context.User_Equipments
                .Include (u => u.User)
                .Include (o => o.User.Office)
                .Include (e => e.Equipment)
                .ToDataSourceResultAsync (request,
                    data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
            if (_rolesService.IsUserInRoleAsync (_uid, "Admin").Result) {
                user_equipments = await context.User_Equipments
                    .Include (u => u.User)
                    .Include (o => o.User.Office)
                    .Include (e => e.Equipment)
                    .ToDataSourceResultAsync (request,
                        data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
            } else if (_rolesService.IsUserInRoleAsync (_uid, "Editor").Result) {
                var _offid = context.Users.FindAsync (_uid).Result.Office_ID;
                user_equipments = await context.User_Equipments
                    .Include (u => u.User)
                    .Include (o => o.User.Office)
                    .Include (e => e.Equipment)
                    .Where (u => u.User.Office_ID == _offid)
                    .ToDataSourceResultAsync (request,
                        data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
            } else if (_rolesService.IsUserInRoleAsync (_uid, "User").Result) {
                user_equipments = await context.User_Equipments
                    .Include (u => u.User)
                    .Include (o => o.User.Office)
                    .Include (e => e.Equipment)
                    .Where (u => u.User_ID == _uid)
                    .ToDataSourceResultAsync (request,
                        data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
            }
            return user_equipments;

        }

        [HttpPost]
        public async Task<IActionResult> CreateUser_Equipment ([FromBody] User_EquipmentResource User_EquipmentResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var user_equipments = mapper.Map<User_EquipmentResource, User_Equipment> (User_EquipmentResource);
            context.User_Equipments.Add (user_equipments);
            await context.SaveChangesAsync ();
            return Ok (user_equipments);
        }

        [HttpPut ("{id}")]
        public async Task<IActionResult> UpdateUser_Equipment (int id, [FromBody] User_EquipmentResource User_EquipmentResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var user_equipments = await context.User_Equipments.FindAsync (id);
            if (user_equipments == null)
                return NotFound ();
            mapper.Map<User_EquipmentResource, User_Equipment> (User_EquipmentResource, user_equipments);
            await context.SaveChangesAsync ();
            return Ok (user_equipments);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> GetUser_Equipment (int id) {
            //var user_equipments = await context.User_Equipments.FindAsync (id);
            var user_equipments = await context.User_Equipments
                .Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).FirstOrDefaultAsync (i => i.UserEquip_ID == id);
            if (user_equipments == null)
                return NotFound ();
            var User_EquipmentResource = mapper.Map<User_Equipment, User_EquipmentResource> (user_equipments);
            return Ok (User_EquipmentResource);
        }

        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteUser_Equipment (int id) {
            var user_equipments = await context.User_Equipments.FindAsync (id);
            if (user_equipments == null)
                return NotFound ();
            context.Remove (user_equipments);
            await context.SaveChangesAsync ();
            return Ok (id);
        }
        // ***************************************************************************************
        private void GetAllLeavesIDs (int? id, List<int> myids) {
            IQueryable<Equipment> ParentFromDatabase;
            if (id == null)
                ParentFromDatabase = context.Equipments.SelectMany (x => x.Children);
            else
                ParentFromDatabase = context.Equipments.Where (x => x.Equip_ID == id).SelectMany (x => x.Children);
            foreach (var child in ParentFromDatabase) {
                if (!myids.Contains (child.Equip_ID))
                    myids.Add (child.Equip_ID);
                GetAllLeavesIDs (child.Equip_ID, myids);
            }
            //	return IDS;
        }
        // ***************************************************************************************
        public List<Equipment> GetChildIDs (int? ID) {
            if (ID.HasValue)
                return context.Equipments.Where (x => x.Parent_Equip_ID == ID).ToList ();
            else
                return context.Equipments.Where (x => x.Parent_Equip_ID == null).ToList ();
        }
        // ---------------------------------------------------------------------------------------
        [HttpGet ("search")]
        public async Task<object> GetUser_Equipment ([DataSourceRequest] DataSourceRequest request, string user_ID, string office_ID, string equipment_ID) {
            // return "{" + $"firstname:{firstName}   lastname:{lastName}    address:{address}" + "}";
            IQueryable<User_Equipment> _user_equipments = context.User_Equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment);
            if (user_ID != null && user_ID != "") {
                int[] idu = user_ID.Split (',').Select (Int32.Parse).ToArray ();
                _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (u => idu.Contains ((int) u.User_ID));
            }
            if (office_ID != null && office_ID != "") {
                int[] ido = office_ID.Split (',').Select (Int32.Parse).ToArray ();
                _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (o => ido.Contains ((int) o.User.Office_ID));
            }
            if (equipment_ID != null && equipment_ID != "") {

                int[] ide = equipment_ID.Split (',').Select (Int32.Parse).ToArray ();
                List<int> IDS = new List<int> ();
                foreach (var id in ide) {
                    GetAllLeavesIDs (id, IDS);
                }
                if (IDS.Count > 0) {
                    _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (e => IDS.Contains ((int) e.Equip_ID));
                } else {
                    _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (e => ide.Contains ((int) e.Equip_ID));
                }
            }
            var Count = await _user_equipments.CountAsync ();
            // var SumAmount = await _user_equipments.SumAsync (s => s.Amount);
            // var SumOfficeWage = await _user_equipments.SumAsync (s => s.OfficeWage);
            // var SumAddedValue = await _user_equipments.SumAsync (s => s.AddedValue);
            var user_equipments = await _user_equipments.ToDataSourceResultAsync (request,
                data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
            // return useractivities;
            var GenericResult = new { user_equipments = user_equipments, Count = Count /*, SumAmount = SumAmount, SumOfficeWage = SumOfficeWage, SumAddedValue = SumAddedValue*/ };
            return GenericResult;
        }

        // // ------Dashboart Search With detail of equipment---------------------------------------------------------------------------------
        // [HttpGet ("dashsearch")]
        // public async Task<object> GetDashboardUser_Equipment ([DataSourceRequest] DataSourceRequest request, string user_ID, string office_ID, string equipment_ID) {

        //     IQueryable<User_Equipment> _user_equipments = context.User_Equipments;

        //     if (user_ID != null && user_ID != "") {
        //         int[] idu = user_ID.Split (',').Select (Int32.Parse).ToArray ();
        //         _user_equipments = _user_equipments.Where (u => idu.Contains ((int) u.User_ID));
        //     }

        //     if (office_ID != null && office_ID != "") {
        //         int[] ido = office_ID.Split (',').Select (Int32.Parse).ToArray ();
        //         _user_equipments = _user_equipments.Where (o => ido.Contains ((int) o.User.Office_ID));
        //     }

        //     if (equipment_ID != null && equipment_ID != "") {

        //         int[] ide = equipment_ID.Split (',').Select (Int32.Parse).ToArray ();
        //         List<int> IDS = new List<int> ();

        //         foreach (var id in ide) {
        //             AllChildIDs (id, IDS);
        //         }
        //         if (IDS.Count > 0) {
        //             _user_equipments = _user_equipments.Where (e => IDS.Contains ((int) e.Equip_ID));
        //         } else {
        //             _user_equipments = _user_equipments.Where (e => ide.Contains ((int) e.Equip_ID));
        //         }
        //     }

        //     IQueryable<DashbortDTO> _tempUser_equipments = _user_equipments.
        //     GroupBy (x => new { Equip_ID = x.Equip_ID, Equip_Name = x.Equipment.Equip_Name },
        //         (key, g) => new DashbortDTO () { Equip_ID = key.Equip_ID, Equip_Name = key.Equip_Name, Count = g.Count () });

        //     int SumCount = 0;
        //     int Count = request.PageSize;
        //     if (request.PageSize != 0) {
        //         Count = await _tempUser_equipments.Take (request.PageSize).CountAsync ();
        //         SumCount = await _tempUser_equipments.Take (request.PageSize).SumAsync (x => x.Count);
        //     } else {
        //         Count = await _tempUser_equipments.CountAsync ();
        //         SumCount = await _tempUser_equipments.SumAsync (x => x.Count);
        //     }
        //     _tempUser_equipments = _tempUser_equipments.OrderByDescending (x => x.Count);

        //     var user_equipments = await _tempUser_equipments.ToDataSourceResultAsync (request,
        //         data => data);
        //     var GenericResult = new { user_equipments = user_equipments, Count = Count, SumCount = SumCount, };
        //     return GenericResult;
        // }

        // ------Dashboart Search With Group By Parent---------------------------------------------------------------------------------
        [HttpGet ("dashsearch")]
        public async Task<object> GetDashboardUser_Equipment ([DataSourceRequest] DataSourceRequest request, string user_ID, string office_ID, string equipment_ID) {

            var _uid = _usersService.GetCurrentUserId ();
            var roleAdmin = _rolesService.IsUserInRoleAsync (_uid, "Admin").Result;
            var roleEditor = _rolesService.IsUserInRoleAsync (_uid, "Editor").Result;
            var roleUser = _rolesService.IsUserInRoleAsync (_uid, "User").Result;
            var _offid = context.Users.FindAsync (_uid).Result.Office_ID;

            IQueryable<User_Equipment> _user_equipments = context.User_Equipments;

            if (user_ID != null && user_ID != "") {
                int[] idu = user_ID.Split (',').Select (Int32.Parse).ToArray ();
                _user_equipments = _user_equipments.Where (u => idu.Contains ((int) u.User_ID));
            }

            if (office_ID != null && office_ID != "") {
                int[] ido = office_ID.Split (',').Select (Int32.Parse).ToArray ();
                List<int> _childido = new List<int> ();
                foreach (var _item in ido) {
                    _childido.AddRange (context.Offices.Where (x => x.Parent_Office_ID == _item).Select (x => x.Office_ID).ToList ());
                }
                _childido.AddRange (ido);
                _user_equipments = _user_equipments.Where (o => _childido.Contains ((int) o.User.Office_ID));
            }
            if (roleEditor)
                _user_equipments = _user_equipments.Where (u => u.User.Office_ID == _offid || u.User.Office.Parent_Office_ID == _offid);
            if (roleUser)
                _user_equipments = _user_equipments.Where (u => u.User_ID == _uid);

            List<int> __ide = new List<int> ();
            List<int?> _ide = new List<int?> ();
            if (equipment_ID != null && equipment_ID != "") {

                __ide = equipment_ID.Split (',').Select (Int32.Parse).ToList ();
                _ide = __ide.Select (i => (int?) i).ToList ();
            } else {
                _ide = new List<int?> { null };
            }

            List<int> AllLeavesIDS = new List<int> ();
            List<Equipment> ChildIDS = new List<Equipment> ();
            List<DashbortDTO> _data = new List<DashbortDTO> ();
            int _count = 0;

            foreach (var id in _ide) {
                ChildIDS = GetChildIDs (id);
                if (ChildIDS.Count () > 0) {
                    foreach (var item in ChildIDS) {
                        AllLeavesIDS = new List<int> ();
                        GetAllLeavesIDs (item.Equip_ID, AllLeavesIDS);
                        if (AllLeavesIDS.Count () > 0) {
                            _count = _user_equipments.Where (x => AllLeavesIDS.Contains (x.Equip_ID)).Count ();
                        } else {
                            _count = _user_equipments.Where (x => x.Equip_ID == item.Equip_ID).Count ();

                        }
                        _data.Add (new DashbortDTO (item.Equip_ID, item.Equip_Name, _count));
                    }
                } else {
                    _count = _user_equipments.Where (x => x.Equip_ID == id).Count ();
                    _data.Add (new DashbortDTO (context.Equipments.First (x => x.Equip_ID == id).Equip_ID, context.Equipments.First (x => x.Equip_ID == id).Equip_Name, _count));
                }
            }
            IQueryable<DashbortDTO> _tempUser_equipments = _data.AsQueryable ();
            int SumCount = 0;
            int Count = request.PageSize;
            if (request.PageSize != 0) {
                Count = _tempUser_equipments.Take (request.PageSize).Count ();
                SumCount = _tempUser_equipments.Take (request.PageSize).Sum (x => x.Count);
            } else {
                Count = _tempUser_equipments.Count ();
                SumCount = _tempUser_equipments.Sum (x => x.Count);
            }
            _tempUser_equipments = _tempUser_equipments.Where (c => c.Count != 0).OrderByDescending (x => x.Count);
            // Random Color For Chart
            // int numColors = _tempUser_equipments.Count ();
            // var Colors = new List<string> ();
            // for (int i = 0; i < numColors; i++) {
            //     var random = new Random (); // Don't put this here!
            //     Colors.Add (String.Format ("#{0:X6}", random.Next (0x1000000)));
            // }
            // _tempUser_equipments.Color = Colors;
            //-------------------------------------------------------------
            var user_equipments = await _tempUser_equipments.ToDataSourceResultAsync (request,
                data => data);
            var GenericResult = new { user_equipments = user_equipments, Count = Count, SumCount = SumCount };
            return GenericResult;
            // جستجوی کاربران اداره ای که وارد شده است
            //     List<int> IDS = new List<int> ();

            //     foreach (var id in ide) {
            //         AllChildIDs (id, IDS);
            //     }
            //     if (IDS.Count > 0) {
            //         _user_equipments = _user_equipments.Where (e => IDS.Contains ((int) e.Equip_ID));
            //     } else {
            //         _user_equipments = _user_equipments.Where (e => ide.Contains ((int) e.Equip_ID));
            //     }
            // }

            // IQueryable<DashbortDTO> _tempUser_equipments = _user_equipments.
            // GroupBy (x => new { Equip_ID = x.Equip_ID, Equip_Name = x.Equipment.Equip_Name },
            //     (key, g) => new DashbortDTO () { Equip_ID = key.Equip_ID, Equip_Name = key.Equip_Name, Count = g.Count () });

            // int SumCount = 0;
            // int Count = request.PageSize;
            // if (request.PageSize != 0) {
            //     Count = await _tempUser_equipments.Take (request.PageSize).CountAsync ();
            //     SumCount = await _tempUser_equipments.Take (request.PageSize).SumAsync (x => x.Count);
            // } else {
            //     Count = await _tempUser_equipments.CountAsync ();
            //     SumCount = await _tempUser_equipments.SumAsync (x => x.Count);
            // }
            // _tempUser_equipments = _tempUser_equipments.OrderByDescending (x => x.Count);

            // var user_equipments = await _tempUser_equipments.ToDataSourceResultAsync (request,
            //     data => data);
            // var GenericResult = new { user_equipments = user_equipments, Count = Count, SumCount = SumCount, };
            // return GenericResult;
        }
    }

}