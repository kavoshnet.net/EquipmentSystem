using System;
using EquipmentSystem.DomainClasses;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class User_EquipmentReqResource {
        public int UserEquipReq_ID { get; set; }
        public string SHSerialReq { get; set; }
        public string SHAmvalReq { get; set; }
        public string Desc1Req { get; set; }
        public string Desc2Req { get; set; }
        public string Desc3Req { get; set; }
        public string Desc4Req { get; set; }
        public string Desc5Req { get; set; }
        public string Desc6Req { get; set; }
        public int? UserEquip_ID { get; set; }
        public virtual User_EquipmentResource Old_User_Equipment { get; set; }
        public int? UserReq_ID { get; set; }
        public virtual UserResource UserReq { get; set; }
        public int? UserDo_ID { get; set; }
        public virtual UserResource UserDo { get; set; }
        public int? EquipReq_ID { get; set; }
        public virtual EquipmentResource EquipmentReq { get; set; }
        public DateTimeOffset? DateReqAction { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
        public ActionEnum? UserRecAction { get; set; }
        public DateTimeOffset? DateDoAction { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ActionEnum? UserDoAction { get; set; }
    }
}