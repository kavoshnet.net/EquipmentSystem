namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class User_EquipmentResource {
        public int UserEquip_ID { get; set; }

        public string SHSerial { get; set; }
        public string SHAmval { get; set; }
        public string Desc1 { get; set; }
        public string Desc2 { get; set; }
        public string Desc3 { get; set; }
        public string Desc4 { get; set; }
        public string Desc5 { get; set; }
        public string Desc6 { get; set; }

        public int User_ID { get; set; }
        public UserResource User { get; set; }

        public int Equip_ID { get; set; }
        public EquipmentResource Equipment { get; set; }
        // public int Office_ID { get; set; }

    }
}