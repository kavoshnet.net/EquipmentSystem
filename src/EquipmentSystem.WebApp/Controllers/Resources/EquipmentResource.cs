using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class EquipmentResource {
        public int Equip_ID { get; set; }
        public int? Parent_Equip_ID { get; set; }

        public string Equip_Name { get; set; }

        // public virtual EquipmentResource Parent { get; set; }
        //  public ICollection<EquipmentResource> Children { get; set; }
        //  public ICollection<EquipmentResource> User_Equipments { get; set; }
        // public EquipmentResource () {
        //      User_Equipments = new Collection<EquipmentResource> ();
        //      Children = new Collection<EquipmentResource> ();
        // }

    }
}