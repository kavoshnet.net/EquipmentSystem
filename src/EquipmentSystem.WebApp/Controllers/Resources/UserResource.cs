using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class UserResource {
        public int User_ID { get; set; }
        public string User_Fname { get; set; }
        public string User_Lname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Office_ID { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset? LastLoggedIn { get; set; }
        public string SerialNumber { get; set; }

        public OfficeResource Office { get; set; }
        // public ICollection<User_EquipmentResource> User_EquipMents { get; set; }
        // public UserResource () {
        //     User_EquipMents = new Collection<User_EquipmentResource> ();
        // }
    }
}