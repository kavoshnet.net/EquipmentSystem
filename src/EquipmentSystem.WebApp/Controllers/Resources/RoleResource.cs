using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class RoleResource {
        public int Role_ID { get; set; }
        public string Role_Name { get; set; }
        public string Role_Caption { get; set; }
        public UserResource User { get; set; }
        // public ICollection<User_EquipmentResource> User_EquipMents { get; set; }
        // public UserResource () {
        //     User_EquipMents = new Collection<User_EquipmentResource> ();
        // }
    }
}