using EquipmentSystem.WebApp.Controllers.Resources;

namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class UserRoleResource {
        public int User_ID { get; set; }
        public int Role_ID { get; set; }
        public UserResource User { get; set; }
        public RoleResource Role { get; set; }

    }
}