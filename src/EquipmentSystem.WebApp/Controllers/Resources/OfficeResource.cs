using System.Collections.Generic;
using System.Collections.ObjectModel;
using EquipmentSystem.DomainClasses;
namespace EquipmentSystem.WebApp.Controllers.Resources {
    public class OfficeResource {
        public int Office_ID { get; set; }
        public string Office_Name { get; set; }
        public string Office_Code { get; set; }
        public int? Parent_Office_ID { get; set; }
        // public ICollection<User_EquipmentResource> User_EquipMents { get; set; }
        // public ICollection<UserResource> Users { get; set; }
        // public OfficeResource () {
        //     Users = new Collection<UserResource> ();
        // }
    }
}