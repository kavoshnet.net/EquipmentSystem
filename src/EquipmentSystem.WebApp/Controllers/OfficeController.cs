using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.Services;
using EquipmentSystem.WebApp.Controllers.Resources;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace EquipmentSystem.WebApp.Controllers
{

    [Route("/api/office")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class OfficeController : Controller
    {
        private readonly IMapper mapper;
        private readonly ApplicationDbContext context;
        private readonly IUsersService _usersService;

        public OfficeController(IMapper mapper, ApplicationDbContext context, IUsersService usersService)
        {
            this.context = context;
            this.mapper = mapper;
            this._usersService = usersService;

        }

        [HttpGet]
        public async Task<DataSourceResult> GetOffice([DataSourceRequest] DataSourceRequest request)
        {
            var office = await context.Offices.Include(u => u.Users).OrderBy(x => x.Office_Name).ToDataSourceResultAsync(request,
                data => mapper.Map<Office, OfficeResource>(data));
            return office;
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOffice([FromBody] OfficeResource officeResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var office = mapper.Map<OfficeResource, Office>(officeResource);
            context.Offices.Add(office);
            await context.SaveChangesAsync();
            return Ok(office);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOffice(int id, [FromBody] OfficeResource officeResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var office = await context.Offices.FindAsync(id);
            if (office == null)
                return NotFound();
            mapper.Map<OfficeResource, Office>(officeResource, office);
            await context.SaveChangesAsync();
            return Ok(office);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOffice(int id)
        {
            var office = await context.Offices.FindAsync(id);
            if (office == null)
                return NotFound();
            var officeResource = mapper.Map<Office, OfficeResource>(office);
            return Ok(officeResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOffice(int id)
        {
            var office = await context.Offices.FindAsync(id);
            if (office == null)
                return NotFound();
            context.Remove(office);
            await context.SaveChangesAsync();
            return Ok(id);
        }

        [HttpGet("/api/office/root")]
        public async Task<DataSourceResult> GetOfficeRoot([DataSourceRequest] DataSourceRequest request)
        {
            var Office_ID = context.Offices.Where(x => x.Parent_Office_ID == null).OrderBy(x => x.Office_Name).Select(x => x.Office_ID).ToList();
            int[] ide = Office_ID.ToArray();
            var office = context.Offices.Where(e => ide.Contains((int)e.Office_ID)).OrderBy(x => x.Office_Name);
            var _office = await office.ToDataSourceResultAsync(request,
                data => mapper.Map<Office, OfficeResource>(data));
            return _office;
        }

        [HttpGet("/api/office/rootuserloggedin")]
        public async Task<DataSourceResult> GetOfficeRootUserLoggedIn([DataSourceRequest] DataSourceRequest request)
        {
            var _uid = _usersService.GetCurrentUserId();
            var _offid = context.Users.FirstOrDefault(u => u.User_ID == _uid).Office_ID;

            var Office_ID = context.Offices.Where(o => o.Office_ID == _offid).Where(x => x.Parent_Office_ID == null).OrderBy(x => x.Office_Name).Select(x => x.Office_ID).ToList();
            int[] ide = Office_ID.ToArray();
            var office = context.Offices.Where(e => ide.Contains((int)e.Office_ID)).OrderBy(x => x.Office_Name);
            var _office = await office.ToDataSourceResultAsync(request,
                data => mapper.Map<Office, OfficeResource>(data));
            return _office;
        }

        [HttpGet("/api/office/parents/{id}")]
        public async Task<List<int?>> GetOfficeParents(int? id, [DataSourceRequest] DataSourceRequest request)
        {
            // var temp = GetAllChild (id);
            List<int?> Office_ID1 = new List<int?>();
            var theid = id;
            var parent = context.Offices.Where(i => i.Office_ID == theid).OrderBy(x => x.Office_Name).Select(i => i.Parent_Office_ID).FirstOrDefault();
            while (parent != null)
            {
                Office_ID1.Add(parent);
                theid = parent;
                parent = context.Offices.Where(i => i.Office_ID == theid).OrderBy(x => x.Office_Name).Select(i => i.Parent_Office_ID).FirstOrDefault();
            }
            return await Office_ID1.Reverse<int?>().ToAsyncEnumerable().ToList();
        }

        [HttpGet("/api/office/child/{id}")]
        public async Task<DataSourceResult> GetOfficesChild(string id, [DataSourceRequest] DataSourceRequest request)
        {
            // var temp = GetAllChild (id);
            List<int> Office_ID1 = new List<int>();
            int[] _ide = id.Split(',').Select(Int32.Parse).ToArray();

            foreach (var _id in _ide)
            {
                Office_ID1.AddRange(context.Offices.Where(x => x.Parent_Office_ID == _id).OrderBy(x => x.Office_Name).Select(x => x.Office_ID).ToList());
            }
            // var Equipment_ID2 = context.Equipments.Where (x => x.Parent_Equip_ID == id).SelectMany (x => x.Children).Select (x => x.Equip_ID).ToList ();
            // // .SelectMany(x => x.Equip_ID).ToList();
            // Equipment_ID1.AddRange (Equipment_ID2);
            var Office_ID = Office_ID1.ToArray();
            int[] ide = Office_ID.ToArray();
            var office = context.Offices.Where(e => ide.Contains((int)e.Office_ID)).OrderBy(x => x.Office_Name);
            // if (equipment == null)
            //     return NotFound ();
            var _office = await office.OrderBy(x => x.Office_Name).ToDataSourceResultAsync(request,
                data => mapper.Map<Office, OfficeResource>(data));
            return _office;

            // var Equipment = await equipment.ToListAsync ();
            // var equipmentResource = mapper.Map<IEnumerable<Equipment>, IEnumerable<EquipmentResource>> (Equipment);
            // return Ok (equipmentResource);
            // return NotFound ();

        }

        // [HttpGet ("search")]
        // public async Task<IEnumerable<OfficeResource>> GetOffice (string office_name, string office_code) {
        //     // return "{" + $"firstname:{firstName}   lastname:{lastName}    address:{address}" + "}";
        //     IQueryable<Office> _office = context.Offices;
        //     if (office_name != null && office_name != "") {
        //         int[] ids = office_name.Split (',').Select (Int32.Parse).ToArray ();
        //         _office = context.Offices.Where (i => ids.Contains (i.Office_ID));
        //     }
        //     if (office_code != null && office_code != "") {
        //         _office = _office.Where (b => b.Office_Code == office_code);
        //     }
        //     var Office = await _office.ToListAsync ();
        //     return mapper.Map<IEnumerable<Office>, IEnumerable<OfficeResource>> (Office);
        // }

    }
}