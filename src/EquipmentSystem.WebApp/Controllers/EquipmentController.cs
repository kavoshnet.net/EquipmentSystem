using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.WebApp.Controllers.Resources;
using EquipmentSystem.WebApp.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EquipmentSystem.WebApp.Controllers {

    public class EquipmentDTO {
        public int Equip_ID { get; set; }
        public int Parent_Equip_ID { get; set; }
        public string Equip_Name { get; set; }
        public List<EquipmentDTO> Children { get; set; }
    }

    [Route ("/api/equipment")]
    [EnableCors ("CorsPolicy")]
    [Authorize]
    public class EquipmentController : Controller {
        private readonly IMapper mapper;
        private readonly ApplicationDbContext context;
        public EquipmentController (IMapper mapper, ApplicationDbContext context) {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<DataSourceResult> GetEquipment ([DataSourceRequest] DataSourceRequest request) {
            var equipment = await context.Equipments.OrderBy (x => x.Equip_Name).ToDataSourceResultAsync (request,
                data => mapper.Map<Equipment, EquipmentResource> (data));
            return equipment;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEquipment ([FromBody] EquipmentResource equipmentResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var equipment = mapper.Map<EquipmentResource, Equipment> (equipmentResource);
            context.Equipments.Add (equipment);
            await context.SaveChangesAsync ();
            return Ok (equipment);
        }

        [HttpPut ("{id}")]
        public async Task<IActionResult> UpdateEquipment (int id, [FromBody] EquipmentResource equipmentResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var equipment = await context.Equipments.FindAsync (id);
            if (equipment == null)
                return NotFound ();
            mapper.Map<EquipmentResource, Equipment> (equipmentResource, equipment);
            await context.SaveChangesAsync ();
            return Ok (equipment);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> GetEquipment (int id) {
            var equipment = await context.Equipments.FindAsync (id);
            if (equipment == null)
                return NotFound ();
            var equipmentResource = mapper.Map<Equipment, EquipmentResource> (equipment);
            return Ok (equipmentResource);
        }

        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteEquipment (int id) {
            var equipment = await context.Equipments.FindAsync (id);
            if (equipment == null)
                return NotFound ();
            context.Remove (equipment);
            await context.SaveChangesAsync ();
            return Ok (id);
        }

        // --------------------------------------------------------------------------
        // private List<EquipmentDTO> GetAllChild (int id) {
        //     return context.Equipments.Where (p => p.Parent_Equip_ID == id).Select (c => new EquipmentDTO () {
        //         Equip_ID = c.Equip_ID,
        //             Equip_Name = c.Equip_Name,
        //             Parent_Equip_ID = c.Parent_Equip_ID,
        //             Children = GetAllChild (c.Equip_ID)
        //     }).ToList ();
        // }
        // private string GetAllChildID (List<EquipmentDTO> hierarchy) {
        //     var s = "";
        //     if (hierarchy != null) {
        //         foreach (var item in hierarchy) {
        //             s += item.Equip_ID.ToString () + "," + GetAllChildID (item.Children).ToString ();
        //         }
        //     }
        //     return s;
        // }
        // public static IEnumerable<T> Flatten<T, R> (this IEnumerable<T> source, Func<T, R> recursion) where R : IEnumerable<T> {
        //     return source.SelectMany (x => (recursion (x) != null && recursion (x).Any ()) ? recursion (x).Flatten (recursion) : null)
        //         .Where (x => x != null);
        // }

        [HttpGet ("/api/equipment/root")]
        public async Task<DataSourceResult> GetEquipmentRoot ([DataSourceRequest] DataSourceRequest request) {
            var Equipment_ID = context.Equipments.Where (x => x.Parent_Equip_ID == null).OrderBy (x => x.Equip_Name).Select (x => x.Equip_ID).ToList ();
            int[] ide = Equipment_ID.ToArray ();
            var equipment = context.Equipments.Where (e => ide.Contains ((int) e.Equip_ID)).OrderBy (x => x.Equip_Name);
            var _equipment = await equipment.ToDataSourceResultAsync (request,
                data => mapper.Map<Equipment, EquipmentResource> (data));
            return _equipment;

            //     if (equipment == null)
            //         return NotFound ();
            //     var Equipment = await equipment.ToListAsync ();
            //     var equipmentResource = mapper.Map<IEnumerable<Equipment>, IEnumerable<EquipmentResource>> (Equipment);
            //     return Ok (equipmentResource);
            // }
            // return NotFound ();

        }

        // -------------------------------------------------------------
        // -------------------------------------------------------------
        // -------------------------------------------------------------
        [HttpGet ("/api/equipment/parents/{id}")]
        public async Task<List<int?>> GetEquipmentParents (int? id, [DataSourceRequest] DataSourceRequest request) {
            // var temp = GetAllChild (id);
            List<int?> Equipment_ID1 = new List<int?> ();
            var theid = id;
            var parent = context.Equipments.Where (i => i.Equip_ID == theid).OrderBy (x => x.Equip_Name).Select (i => i.Parent_Equip_ID).FirstOrDefault ();
            while (parent != null) {
                Equipment_ID1.Add (parent);
                theid = parent;
                parent = context.Equipments.Where (i => i.Equip_ID == theid).OrderBy (x => x.Equip_Name).Select (i => i.Parent_Equip_ID).FirstOrDefault ();
            }
            return await Equipment_ID1.Reverse<int?> ().ToAsyncEnumerable ().ToList ();
        }

        // -------------------------------------------------------------
        // -------------------------------------------------------------
        // -------------------------------------------------------------
        [HttpGet ("/api/equipment/child/{id}")]
        public async Task<DataSourceResult> GetEquipmentChild (string id, [DataSourceRequest] DataSourceRequest request) {
            // var temp = GetAllChild (id);
            List<int> Equipment_ID1 = new List<int> ();
            int[] _ide = id.Split (',').Select (Int32.Parse).ToArray ();

            foreach (var _id in _ide) {
                Equipment_ID1.AddRange (context.Equipments.Where (x => x.Equip_ID == _id).OrderBy (x => x.Equip_Name).SelectMany (x => x.Children).Select (x => x.Equip_ID).ToList ());
            }
            // var Equipment_ID2 = context.Equipments.Where (x => x.Parent_Equip_ID == id).SelectMany (x => x.Children).Select (x => x.Equip_ID).ToList ();
            // // .SelectMany(x => x.Equip_ID).ToList();
            // Equipment_ID1.AddRange (Equipment_ID2);
            var Equipment_ID = Equipment_ID1.ToArray ();
            int[] ide = Equipment_ID.ToArray ();
            var equipment = context.Equipments.Where (e => ide.Contains ((int) e.Equip_ID)).OrderBy (x => x.Equip_Name);
            // if (equipment == null)
            //     return NotFound ();
            var _equipment = await equipment.OrderBy (x => x.Equip_Name).ToDataSourceResultAsync (request,
                data => mapper.Map<Equipment, EquipmentResource> (data));
            return _equipment;

            // var Equipment = await equipment.ToListAsync ();
            // var equipmentResource = mapper.Map<IEnumerable<Equipment>, IEnumerable<EquipmentResource>> (Equipment);
            // return Ok (equipmentResource);
            // return NotFound ();

        }

        // [HttpGet ("search")]
        // public async Task<IEnumerable<EquipmentResource>> GetEquipment (string equipment_name, string equipment_code) {
        //     // return "{" + $"firstname:{firstName}   lastname:{lastName}    address:{address}" + "}";
        //     IQueryable<Equipment> _equipment = context.Equipments;
        //     if (equipment_name != null && equipment_name != "") {
        //         int[] ids = equipment_name.Split (',').Select (Int32.Parse).ToArray ();
        //         _equipment = context.Equipments.Where (i => ids.Contains (i.Equipment_ID));
        //     }
        //     if (equipment_code != null && equipment_code != "") {
        //         _equipment = _equipment.Where (b => b.Equipment_Code == equipment_code);
        //     }
        //     var Equipment = await _equipment.ToListAsync ();
        //     return mapper.Map<IEnumerable<Equipment>, IEnumerable<EquipmentResource>> (Equipment);
        // }

    }
}