using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.Services;
using EquipmentSystem.WebApp.Controllers.Resources;
using EquipmentSystem.WebApp.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EquipmentSystem.WebApp.Controllers {
    [Route ("api/user")]
    [EnableCors ("CorsPolicy")]
    [Authorize]
    public class UserController : Controller {
        private readonly IMapper mapper;
        private readonly ApplicationDbContext context;
        private readonly ISecurityService _securityService;
        private readonly IUsersService _usersService;

        public UserController (IMapper mapper, ApplicationDbContext context, ISecurityService securityService, IUsersService usersService) {
            this.mapper = mapper;
            this.context = context;
            _securityService = securityService;
            this._usersService = usersService;

        }

        [HttpGet]
        public async Task<DataSourceResult> GetUser ([DataSourceRequest] DataSourceRequest request) {
            var user = await context.Users.Include (o => o.Office).Include (ue => ue.User_Equipments).OrderBy (x => x.User_Lname).ToDataSourceResultAsync (request,
                data => mapper.Map<User, UserResource> (data));
            return user;
        }

        [HttpGet ("office/getuserof_office_userloggedin")]
        public async Task<DataSourceResult> GetUserOf_Office_UserLoggedIn ([DataSourceRequest] DataSourceRequest request) {
            var _uid = _usersService.GetCurrentUserId ();
            var _offid = context.Users.FirstOrDefaultAsync (u => u.User_ID == _uid).Result.Office_ID;
            var user = await context.Users.Include (o => o.Office).Include (ue => ue.User_Equipments).Where(o=>o.Office_ID == _offid). OrderBy (x => x.User_Lname).ToDataSourceResultAsync (request,
                data => mapper.Map<User, UserResource> (data));
            return user;
        }

        [HttpGet ("office/{office_ID}")]
        public async Task<DataSourceResult> GetUsersOffices (string office_ID, [DataSourceRequest] DataSourceRequest request) {
            IQueryable<User> user = context.Users.Include (o => o.Office).Include (ue => ue.User_Equipments);
            if (office_ID != null && office_ID != "") {
                int[] ido = office_ID.Split (',').Select (Int32.Parse).ToArray ();
                List<int> _childido = new List<int> ();
                foreach (var _item in ido) {
                    _childido.AddRange (context.Offices.Where (x => x.Parent_Office_ID == _item).Select (x => x.Office_ID).ToList ());
                }
                _childido.AddRange (ido);
                user = user.Where (o => _childido.Contains ((int) o.Office_ID));
            }
            var _user = await user.ToDataSourceResultAsync (request,
                data => mapper.Map<User, UserResource> (data));
            return _user;

        }

        [HttpPost]
        public async Task<IActionResult> CreateUser ([FromBody] UserResource UserResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var user = mapper.Map<UserResource, User> (UserResource);
            user.SerialNumber = Guid.NewGuid ().ToString ("N");
            user.Password = _securityService.GetSha256Hash (user.Password);
            user.LastLoggedIn = DateTime.Now;
            context.Users.Add (user);
            await context.SaveChangesAsync ();
            return Ok (user);
        }

        [HttpPut ("{id}")]
        public async Task<IActionResult> UpdateUser (int id, [FromBody] UserResource UserResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var user = await context.Users.FindAsync (id);
            if (user == null)
                return NotFound ();
            mapper.Map<UserResource, User> (UserResource, user);
            await context.SaveChangesAsync ();
            return Ok (user);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> GetUser (int id) {
            var user = await context.Users
                .Include (o => o.Office)
                .Where (u => u.User_ID == id)
                .FirstOrDefaultAsync ();
            if (user == null)
                return NotFound ();
            var UserResource = mapper.Map<User, UserResource> (user);
            return Ok (UserResource);
        }

        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteUser (int id) {
            var user = await context.Users.FindAsync (id);
            if (user == null)
                return NotFound ();
            context.Remove (user);
            await context.SaveChangesAsync ();
            return Ok (id);
        }
    }
}