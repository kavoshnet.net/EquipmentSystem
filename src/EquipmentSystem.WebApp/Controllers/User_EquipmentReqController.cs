using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentSystem.DataLayer.Context;
using EquipmentSystem.DomainClasses;
using EquipmentSystem.Services;
using EquipmentSystem.WebApp.Controllers.Resources;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace EquipmentSystem.WebApp.Controllers {

  [Route ("api/user_equipmentreq")]
  [EnableCors ("CorsPolicy")]
  [Authorize]
  public class User_EquipmentReqController : Controller {

    private readonly IMapper mapper;
    private readonly ApplicationDbContext context;
    private readonly IUsersService _usersService;
    private readonly IRolesService _rolesService;

    public User_EquipmentReqController (
      IMapper mapper,
      ApplicationDbContext context,
      IUsersService usersService,
      IRolesService rolesService
    ) {

      this.mapper = mapper;
      this.context = context;
      this._usersService = usersService;
      this._rolesService = rolesService;

    }

    [HttpGet]
    public async Task<DataSourceResult> GetUser_EquipmentReq ([DataSourceRequest] DataSourceRequest request) {

      var _uid = _usersService.GetCurrentUserId ();
      // var roleName = _rolesService.IsUserInRoleAsync (_uid, "Admin");
      // var ID = User.FindFirst (ClaimTypes.UserData)?.Value;
      // var _uid = long.Parse (ID);
      DataSourceResult user_equipmentReqs = null;
      user_equipmentReqs = await context.User_EquipmentReqs
        .Include (u => u.UserReq)
        .Include (u => u.UserDo)
        .Include (o => o.UserReq.Office)
        .Include (o => o.UserDo.Office)
        .Include (e => e.EquipmentReq)
        .ToDataSourceResultAsync (request,
          data => mapper.Map<User_EquipmentReq, User_EquipmentReqResource> (data));
      if (_rolesService.IsUserInRoleAsync (_uid, "Admin").Result) {
        user_equipmentReqs = await context.User_EquipmentReqs
          .Include (u => u.UserReq)
          .Include (u => u.UserDo)
          .Include (o => o.UserReq.Office)
          .Include (o => o.UserDo.Office)
          .Include (e => e.EquipmentReq)
          .ToDataSourceResultAsync (request,
            data => mapper.Map<User_EquipmentReq, User_EquipmentReqResource> (data));
      } else if (_rolesService.IsUserInRoleAsync (_uid, "Editor").Result) {
        var _offid = context.Users.FindAsync (_uid).Result.Office_ID;
        user_equipmentReqs = await context.User_EquipmentReqs
          .Include (u => u.UserReq)
          .Include (u => u.UserDo)
          .Include (o => o.UserReq.Office)
          .Include (o => o.UserDo.Office)
          .Include (e => e.EquipmentReq)
          .Where (u => u.UserReq.Office_ID == _offid)
          .ToDataSourceResultAsync (request,
            data => mapper.Map<User_EquipmentReq, User_EquipmentReqResource> (data));
      } else if (_rolesService.IsUserInRoleAsync (_uid, "User").Result) {
        user_equipmentReqs = await context.User_EquipmentReqs
          .Include (u => u.UserReq)
          .Include (u => u.UserDo)
          .Include (o => o.UserReq.Office)
          .Include (o => o.UserDo.Office)
          .Include (e => e.EquipmentReq)
          .Where (u => u.UserReq_ID == _uid)
          .ToDataSourceResultAsync (request,
            data => mapper.Map<User_EquipmentReq, User_EquipmentReqResource> (data));
      }
      return user_equipmentReqs;

    }

    //TODO: OK is changed
    [HttpPost]
    public async Task<IActionResult> CreateUser_EquipmentReq ([FromBody] User_EquipmentReqResource User_EquipmentReqResource) {
      if (!ModelState.IsValid)
        return BadRequest (ModelState);
      var _uid = _usersService.GetCurrentUserId ();

      var user_equipmentReqs = mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource);
      user_equipmentReqs.UserReq_ID = _uid;
      user_equipmentReqs.DateReqAction = DateTime.Now;
      user_equipmentReqs.UserRecAction = ActionEnum.Insert;
      context.User_EquipmentReqs.Add (user_equipmentReqs);
      await context.SaveChangesAsync ();
      return Ok (user_equipmentReqs);
    }

    [HttpPut ("{id}")]
    public async Task<IActionResult> UpdateUser_EquipmentReq (int id, [FromBody] User_EquipmentReqResource User_EquipmentReqResource) {
      if (!ModelState.IsValid)
        return BadRequest (ModelState);
      var _uid = _usersService.GetCurrentUserId ();
      // var user_equipmentReqs = await context.User_EquipmentReqs.FindAsync (id);
      // if (user_equipmentReqs == null)
      //     return NotFound ();
      var user_equipmentReqs = mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource);
      // mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource, user_equipmentReqs);
      user_equipmentReqs.UserReq_ID = _uid;
      user_equipmentReqs.DateReqAction = DateTime.Now;
      user_equipmentReqs.UserRecAction = ActionEnum.Update;
      context.User_EquipmentReqs.Add (user_equipmentReqs);
      await context.SaveChangesAsync ();
      return Ok (user_equipmentReqs);
    }

    [HttpGet ("{id}")]
    public async Task<IActionResult> GetUser_EquipmentReq (int id) {
      //var user_equipments = await context.User_Equipments.FindAsync (id);
      var user_equipments = await context.User_Equipments
        .Include (u => u.User)
        .Include (o => o.User.Office)
        .Include (e => e.Equipment)
        .FirstOrDefaultAsync (i => i.UserEquip_ID == id);
      if (user_equipments == null)
        return NotFound ();
      var User_EquipmentReqResource = mapper.Map<User_Equipment, User_EquipmentReqResource> (user_equipments);
      return Ok (User_EquipmentReqResource);
    }

    [HttpGet]
    // [Route ("api/reqdetail/{id}")]
    [Route ("reqdetail/{id}")]
    public async Task<IActionResult> GetUser_EquipmentReqReq (int id) {
      //var user_equipments = await context.User_Equipments.FindAsync (id);
      var user_equipmentReqs = await context.User_EquipmentReqs
        .Include (u => u.UserReq)
        .Include (o => o.UserReq.Office)
        .Include (u => u.UserDo)
        .Include (o => o.UserDo.Office)
        .Include (e => e.EquipmentReq)
        .Include (e => e.Old_User_Equipment)
        .Include (e => e.Old_User_Equipment.Equipment)
        .FirstOrDefaultAsync (i => i.UserEquipReq_ID == id);
      if (user_equipmentReqs == null)
        return NotFound ();
      var User_EquipmentReqResource = mapper.Map<User_EquipmentReq, User_EquipmentReqResource> (user_equipmentReqs);
      return Ok (User_EquipmentReqResource);
    }

    [HttpDelete ("{id}")]
    public async Task<IActionResult> DeleteUser_EquipmentReq (int id) {
      var user_equipments = await context.User_Equipments.FindAsync (id);
      if (user_equipments == null)
        return NotFound ();
      var _uid = _usersService.GetCurrentUserId ();
      var User_EquipmentReqResource = mapper.Map<User_Equipment, User_EquipmentReqResource> (user_equipments);
      User_EquipmentReqResource.UserReq_ID = _uid;
      User_EquipmentReqResource.DateReqAction = DateTime.Now;
      User_EquipmentReqResource.UserRecAction = ActionEnum.Delete;

      var user_equipmentReqs = mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource);

      context.User_EquipmentReqs.Add (user_equipmentReqs);
      await context.SaveChangesAsync ();
      return Ok (user_equipmentReqs);

      // context.Remove (user_equipments);
      // await context.SaveChangesAsync ();
      // return Ok (id);
    }

    // ***************************************************************************************
    private void GetAllLeavesIDs (int? id, List<int> myids) {
      IQueryable<Equipment> ParentFromDatabase;
      if (id == null)
        ParentFromDatabase = context.Equipments.SelectMany (x => x.Children);
      else
        ParentFromDatabase = context.Equipments.Where (x => x.Equip_ID == id).SelectMany (x => x.Children);
      foreach (var child in ParentFromDatabase) {
        if (!myids.Contains (child.Equip_ID))
          myids.Add (child.Equip_ID);
        GetAllLeavesIDs (child.Equip_ID, myids);
      }
      //	return IDS;
    }
    // ***************************************************************************************
    // ---------------------------------------------------------------------------------------
    [HttpGet ("search")]
    public async Task<object> GetUser_EquipmentReq ([DataSourceRequest] DataSourceRequest request, string user_ID, string office_ID, string equipment_ID) {
      // return "{" + $"firstname:{firstName}   lastname:{lastName}    address:{address}" + "}";
      IQueryable<User_Equipment> _user_equipments = context.User_Equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment);
      if (user_ID != null && user_ID != "") {
        int[] idu = user_ID.Split (',').Select (Int32.Parse).ToArray ();
        _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (u => idu.Contains ((int) u.User_ID));
      }
      if (office_ID != null && office_ID != "") {
        int[] ido = office_ID.Split (',').Select (Int32.Parse).ToArray ();
        _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (o => ido.Contains ((int) o.User.Office_ID));
      }
      if (equipment_ID != null && equipment_ID != "") {

        int[] ide = equipment_ID.Split (',').Select (Int32.Parse).ToArray ();
        List<int> IDS = new List<int> ();
        foreach (var id in ide) {
          GetAllLeavesIDs (id, IDS);
        }
        if (IDS.Count > 0) {
          _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (e => IDS.Contains ((int) e.Equip_ID));
        } else {
          _user_equipments = _user_equipments.Include (u => u.User).Include (o => o.User.Office).Include (e => e.Equipment).Where (e => ide.Contains ((int) e.Equip_ID));
        }
      }
      var Count = await _user_equipments.CountAsync ();
      // var SumAmount = await _user_equipments.SumAsync (s => s.Amount);
      // var SumOfficeWage = await _user_equipments.SumAsync (s => s.OfficeWage);
      // var SumAddedValue = await _user_equipments.SumAsync (s => s.AddedValue);
      var user_equipments = await _user_equipments.ToDataSourceResultAsync (request,
        data => mapper.Map<User_Equipment, User_EquipmentResource> (data));
      // return useractivities;
      var GenericResult = new { user_equipments = user_equipments, Count = Count /*, SumAmount = SumAmount, SumOfficeWage = SumOfficeWage, SumAddedValue = SumAddedValue*/ };
      return GenericResult;
    }

    // ---------------------------------------------------------------------------------------
    [HttpPost]
    [Route ("reqdetail")]
    public async Task<IActionResult> InsertReq_EquipmentReq ([FromBody] User_EquipmentReqResource User_EquipmentReqResource) {
      if (!ModelState.IsValid)
        return BadRequest (ModelState);
      var _uid = _usersService.GetCurrentUserId ();

      var user_equipment = mapper.Map<User_EquipmentReqResource, User_Equipment> (User_EquipmentReqResource);
      context.User_Equipments.Add (user_equipment);

      var user_equipmentreqs = await context.User_EquipmentReqs.FindAsync (User_EquipmentReqResource.UserEquipReq_ID);
      if (user_equipmentreqs == null)
        return NotFound ();

      mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource, user_equipmentreqs);
      user_equipmentreqs.UserDo_ID = _uid;
      user_equipmentreqs.DateDoAction = DateTime.Now;
      user_equipmentreqs.UserDoAction = ActionEnum.Register;
      await context.SaveChangesAsync ();
      return Ok (user_equipmentreqs);
    }

    // ---------------------------------------------------------------------------------------
    [HttpPut]
    [Route ("reqdetail")]
    public async Task<IActionResult> CheckUpdateReq_EquipmentReq ([FromBody] User_EquipmentReqResource User_EquipmentReqResource, string state) {

      if (state != null)
        return await RejectReq_EquipmentReq (User_EquipmentReqResource.UserEquipReq_ID, state);
      else
        return await UpdateReq_EquipmentReq (User_EquipmentReqResource);
    }

    // ---------------------------------------------------------------------------------------
    [HttpDelete]
    [Route ("reqdetail/{id}")]
    public async Task<IActionResult> DeleteReq_EquipmentReq (int id) {
      var _uid = _usersService.GetCurrentUserId ();
      var user_equipmentreqs = await context.User_EquipmentReqs.FindAsync (id);
      if (user_equipmentreqs == null)
        return NotFound ();

      user_equipmentreqs.UserDo_ID = _uid;
      user_equipmentreqs.DateDoAction = DateTime.Now;
      user_equipmentreqs.UserDoAction = ActionEnum.Register;

      var user_equipment = await context.User_Equipments.FindAsync (user_equipmentreqs.UserEquip_ID);
      if (user_equipment == null)
        return NotFound ();
      context.Remove (user_equipment);
      await context.SaveChangesAsync ();

      return Ok (user_equipmentreqs);
    }

    // ---------------------------------------------------------------------------------------
    private async Task<IActionResult> RejectReq_EquipmentReq (int id, string state) {
      var _uid = _usersService.GetCurrentUserId ();
      var user_equipmentreqs = await context.User_EquipmentReqs.FindAsync (id);
      if (user_equipmentreqs == null)
        return NotFound ();
      if (state.Equals ("reject")) {
        user_equipmentreqs.UserDo_ID = _uid;
        user_equipmentreqs.DateDoAction = DateTime.Now;
        user_equipmentreqs.UserDoAction = ActionEnum.Reject;
      }
      await context.SaveChangesAsync ();

      return Ok (user_equipmentreqs);
    }

    // ---------------------------------------------------------------------------------------
    private async Task<IActionResult> UpdateReq_EquipmentReq (User_EquipmentReqResource User_EquipmentReqResource) {

      if (!ModelState.IsValid)
        return BadRequest (ModelState);
      var _uid = _usersService.GetCurrentUserId ();

      var user_equipment = await context.User_Equipments.FindAsync (User_EquipmentReqResource.UserEquip_ID);
      if (user_equipment == null)
        return NotFound ();
      mapper.Map<User_EquipmentReqResource, User_Equipment> (User_EquipmentReqResource, user_equipment);

      var user_equipmentreqs = await context.User_EquipmentReqs.FindAsync (User_EquipmentReqResource.UserEquipReq_ID);
      if (user_equipmentreqs == null)
        return NotFound ();

      mapper.Map<User_EquipmentReqResource, User_EquipmentReq> (User_EquipmentReqResource, user_equipmentreqs);
      user_equipmentreqs.UserDo_ID = _uid;
      user_equipmentreqs.DateDoAction = DateTime.Now;
      user_equipmentreqs.UserDoAction = ActionEnum.Register;
      await context.SaveChangesAsync ();
      return Ok (user_equipmentreqs);
    }

  }

}